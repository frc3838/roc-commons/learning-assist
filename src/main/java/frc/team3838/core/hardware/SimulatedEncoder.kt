package frc.team3838.core.hardware

import java.util.concurrent.TimeUnit

/**
 * A simple class to simulate an encoder. It simply returns the number of seconds since reset as the distance (in inches).
 */
class SimulatedEncoder
{
    private var start: Long = 0

    fun reset()
    {
        start = System.currentTimeMillis()
    }

    fun getDistance()
    {
        TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start)
    }

}