package frc.team3838.core.hardware

import frc.team3838.core.meta.API
import java.util.concurrent.TimeUnit

/**
 * A simple class to simulate an encoder. It simply returns waits the configured number of seconds
 * before the toggling the get() result. As such, unlike a normal limit switch, this one
 * requires the simulation to be reset between 'uses'.
 */
@API
class SimulatedLimitSwitch
{
    private var start: Long = 0
    private var startingState = false
    private var delayInMills: Long = 1_000

    /**
     * Resets the simulation.
     * @param startingState the starting sate of the limit switch
     * @param seconds the number of seconds to wait before [get] returns the opposite of the [startingState]
     *
     */
    @API
    fun resetSimulation(startingState: Boolean, seconds: Int)
    {
        this.startingState = startingState
        this.delayInMills = TimeUnit.SECONDS.toMillis(seconds.toLong())
        start = System.currentTimeMillis()
    }

    @API
    fun get(): Boolean
    {
        return if ((System.currentTimeMillis() - start) >= delayInMills ) !startingState  else startingState
    }
}