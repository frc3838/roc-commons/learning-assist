package frc.team3838.core.hardware

import edu.wpi.first.wpilibj.SpeedController
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.logging.PeriodicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.utils.format
import mu.KotlinLogging
import org.slf4j.event.Level

/**
 * A mock SpeedController that merely logs the speed settings.
 * A Laconic Logger is used to log the speed when the set method is called.
 * If a `periodicSpeedLoggingInternal` is set, then the speed is also periodically logged.
 */
@Suppress("MemberVisibilityCanBePrivate")
@API
class MockSpeedController @JvmOverloads constructor(val name: String, laconicLoggingInterval: TimeDuration = TimeDuration.ofSeconds(2), periodicSpeedLoggingInternal: TimeDuration? = null): SpeedController
{
    /** Creates a MockSpeedController along with setting the  periodicSpeedLoggingInternal. */
    @API
    constructor(periodicSpeedLoggingInternal: TimeDuration, name: String): this(name, periodicSpeedLoggingInternal = periodicSpeedLoggingInternal)

    private val logger = KotlinLogging.logger {}

    private val setLaconicLogger = LaconicLogger(logger, laconicLoggingInterval)
    private val stopLaconicLogger = LaconicLogger(logger, laconicLoggingInterval)
    private val pidWriteLaconicLogger = LaconicLogger(logger, laconicLoggingInterval)

    private var inverted = false

    private var speed = 0.0


    @Suppress("unused")
    private val periodicLogger: PeriodicLogger? = if (periodicSpeedLoggingInternal == null) null else  PeriodicLogger(logger, Level.INFO, laconicLoggingInterval) {
        "$name current speed: ${speed.format(3)}"
    }


    override fun pidWrite(output: Double)
    {
        pidWriteLaconicLogger.info { "$name pidWrite of ${output.format(3)}" }
    }

    override fun set(speed: Double)
    {
        this.speed = speed
        setLaconicLogger.info{ "$name set to speed of ${speed.format(3)}" }
    }

    override fun get(): Double = speed

    override fun setInverted(isInverted: Boolean)
    {
        logger.info { "$name inverted set to '$isInverted'" }
        inverted = isInverted
    }

    override fun getInverted(): Boolean = inverted

    override fun disable()
    {
        logger.info { "$name has been disabled" }
        speed = 0.0
    }

    override fun stopMotor()
    {
        speed = 0.0
        stopLaconicLogger.info{ "$name has been stopped" }
    }
}