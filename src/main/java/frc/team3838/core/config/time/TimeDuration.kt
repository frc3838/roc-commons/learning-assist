@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package frc.team3838.core.config.time

import com.google.errorprone.annotations.Immutable
import mu.KotlinLogging
import org.jetbrains.annotations.Contract
import java.util.*
import java.util.concurrent.TimeUnit
import javax.annotation.concurrent.ThreadSafe

private val logger = KotlinLogging.logger {}


// TODO: Document: Need more documentation and examples

/**
 * A class to represent a time duration, specifically a time setting such as a timeout, to make it such
 * values easier to "pass around" (i.e. return form a method) or to
 * configure such values, especially in Spring. A number of methods in the Java API, especially in the
 * concurrent API, take a time based setting, to configure things such as a timeout value, as two parameters,
 * a long, representing a duration, and a TimeUnit. However, in certain instances, it is significantly easier
 * to represent these values as a single object especially when used as a return type, as a default value, or for
 * setting a value on a Spring bean (i.e. having and calling 1 setter rather than two). While the Apache Commons Lang
 * or Kotlin `Pair` classes could be used to generically do this, using this `TimeDuration` class makes the meaning of
 * the pair clearer, and adds some additional functionality such as conversions and parsing of text
 * representations of Time Durations. This is especially useful in Spring (and other) XML configuration files.
 * Rather than needing complex XML elements to create a Pair, the timeout value can be represented as a String
 * in the form or "500 milliseconds", "3 seconds", or even "5 minutes, 15 seconds, and 500 milliseconds".
 *
 * This `TimeDuration` class differs from the JodaTime `Duration` and the Java Time API `Duration` in that it allows for
 * easier construction in configuration files (such as Spring XML files) and easier decomposition to the `long duration` and `timeUnit TimeUnit`
 * components so frequently needed in the Java Concurrent API and other methods setting timeout values. Extension
 * functions are provided to convert to and from the JodaTime and Java Duration classes. *
 *
 * The TimeDuration class provides a number of factory methods, most using the naming convention of `of`, for creating instances.
 * See the examples below.
 *
 * @sample TimeDurationExampleKotlin.creatingTimeDurationsInKotlinExample
 * @sample TimeDurationExamplesJava.creatingTimeDurationsInJava
 *
 */
@Immutable
@ThreadSafe
class TimeDuration internal constructor(timeDurationData: TimeDurationData) : Comparable<TimeDuration>
{
    private val timeDurationData: TimeDurationData = if (timeDurationData.hasBeenNormalized) timeDurationData else normalizeToCoarsestTimeUnit(timeDurationData)
    val duration: Long get() = timeDurationData.duration
    val timeUnit: TimeUnit get() = timeDurationData.timeUnit

    fun clone(): TimeDuration = TimeDuration(TimeDurationData(this.timeDurationData.duration, this.timeDurationData.timeUnit))

    override fun compareTo(other: TimeDuration): Int = timeDurationData.compareTo(other.timeDurationData)

    operator fun plus(other: TimeDuration) = of(this, other)
    operator fun minus(subtrahend: TimeDuration) = of(this.toFinestDuration() - subtrahend.toFinestDuration(), finestTimeUnit)

    fun toNanos(): Long = timeUnit.toNanos(duration)
    fun toMicros(): Long = timeUnit.toMicros(duration)
    fun toMillis(): Long = timeUnit.toMillis(duration)
    fun toSeconds(): Long = timeUnit.toSeconds(duration)
    fun toFractionalSeconds(): Double = timeUnit.toNanos(duration).toDouble() / TimeUnit.SECONDS.toNanos(1)
    fun toMinutes(): Long = timeUnit.toMinutes(duration)
    fun toHours(): Long = timeUnit.toHours(duration)
    fun toDays(): Long = timeUnit.toDays(duration)
    fun asJavaDuration(): java.time.Duration = java.time.Duration.ofNanos(toNanos())
    fun asJodaDuration(): org.joda.time.Duration = org.joda.time.Duration.millis(toMillis())


    /**
     * An equals method that requires that that both the duration and TimeUnit of two TimeDurations are equal
     * in order for two TimeDurations to be equal, even if the TimeDurations represent an equivalent period of
     * time. For example {@code TimeDuration("2 DAYS")} would not equal {@code TimeDuration("48 HOURS")}. The
     * standard {@link #equals(Object)} would consider 2 DAYS as equal to 48 HOURS
     *
     * @param other the reference object with which to compare.
     *
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     *
     * @see #equals(Object)
     * @see Object#equals(Object)
     */
    fun equalsPure(other: Any?): Boolean
    {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TimeDuration

        if (duration != other.duration) return false
        if (timeUnit != other.timeUnit) return false

        return true
    }

    /**
     * Compares two TimeDurations for equal/equivalence time period representations ignoring the TimeUnits such
     * that 2 DAYS would be equal to 48 HOURS. For an equals method that requires all properties to be equal
     * such that 2 DAYS would not be equal to 48 HOURS, see {@link #equalsPure(Object)}.
     *
     * @param other the reference object with which to compare.
     *
     * @return {@code true} if this object is represents the same period of time as the obj argument; {@code false} otherwise.
     * @see #equalsPure(Object)
     */
    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TimeDuration
        return this.toFinestDuration() == other.toFinestDuration()
    }

    override fun hashCode(): Int
    {
        var result = duration.hashCode()
        result = 31 * result + timeUnit.hashCode()
        return result
    }

    override fun toString(): String
    {
        val s = "$duration ${timeUnit.name.toLowerCase()}"
        return if (duration != 1L) s else s.removeSuffix("s")
    }

    fun toStringFormatted(): String = org.joda.time.format.PeriodFormat.getDefault().print(asJodaDuration().toPeriod());


    companion object
    {
        @JvmStatic
        fun ofNanos(nanoseconds: Long): TimeDuration = TimeDuration(TimeDurationData(nanoseconds, TimeUnit.NANOSECONDS))
        @JvmStatic
        fun ofMicros(microseconds: Long): TimeDuration = TimeDuration(TimeDurationData(microseconds, TimeUnit.MICROSECONDS))
        @JvmStatic
        fun ofMillis(milliseconds: Long): TimeDuration = TimeDuration(TimeDurationData(milliseconds, TimeUnit.MILLISECONDS))
        @JvmStatic
        fun ofSeconds(seconds: Long): TimeDuration = TimeDuration(TimeDurationData(seconds, TimeUnit.SECONDS))
        @JvmStatic
        fun ofSeconds(vararg partialSeconds: PartialSeconds) =
            TimeDuration(TimeDurationData(PartialSeconds.sumToMillis(0, *partialSeconds), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofSeconds(seconds: Long, vararg partialSeconds: PartialSeconds) =
            TimeDuration(TimeDurationData(PartialSeconds.sumToMillis(seconds, *partialSeconds), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofSeconds(fractionalSeconds: Double) =
            TimeDuration(TimeDurationData((fractionalSeconds * TimeUnit.SECONDS.toMillis(1)).toLong(), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofSecondsAndMs(seconds: Long, ms: Long) = TimeDuration(TimeDurationData(TimeUnit.SECONDS.toMillis(seconds) + ms, TimeUnit.MILLISECONDS))
        @JvmStatic
        fun ofMinutes(minutes: Long): TimeDuration = TimeDuration(TimeDurationData(minutes, TimeUnit.MINUTES))
        @JvmStatic
        fun ofMinutes(vararg partialMinutes: PartialMinutes): TimeDuration =
            TimeDuration(TimeDurationData(PartialMinutes.sumToMillis(0, *partialMinutes), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofMinutes(minutes: Long, vararg partialMinutes: PartialMinutes): TimeDuration =
            TimeDuration(TimeDurationData(PartialMinutes.sumToMillis(minutes, *partialMinutes), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofMinutesAndSeconds(minutes: Long, seconds: Long, vararg partialSeconds: PartialSeconds): TimeDuration =
            TimeDuration(TimeDurationData(TimeUnit.MINUTES.toMillis(minutes) + PartialSeconds.sumToMillis(seconds, *partialSeconds), TimeUnit.MILLISECONDS))

        @JvmStatic
        fun ofHours(hours: Long): TimeDuration = TimeDuration(TimeDurationData(hours, TimeUnit.HOURS))
        @JvmStatic
        fun ofDays(days: Long): TimeDuration = TimeDuration(TimeDurationData(days, TimeUnit.DAYS))
        @JvmStatic
        fun ofSum(timeDurations: Iterable<TimeDuration>): TimeDuration = of(timeDurations)
        @JvmStatic
        fun ofSum(vararg timeDurations: TimeDuration): TimeDuration = of(timeDurations.asList())
        @JvmStatic
        fun of(duration: Long, timeUnit: TimeUnit): TimeDuration = TimeDuration(TimeDurationData(duration, timeUnit))
        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit): TimeDuration =
            TimeDuration(TimeDurationData((finestTimeUnit.convert(duration1, timeUnit1) + finestTimeUnit.convert(duration2, timeUnit2)), finestTimeUnit))

        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, duration3: Long, timeUnit3: TimeUnit): TimeDuration = TimeDuration(
            TimeDurationData(
                (finestTimeUnit.convert(duration1, timeUnit1) + finestTimeUnit.convert(duration2, timeUnit2) + finestTimeUnit.convert(
                    duration3,
                    timeUnit3
                                                                                                                                     )), finestTimeUnit
                            )
                                                                                                                                                             )

        @JvmStatic
        fun of(
            duration1: Long,
            timeUnit1: TimeUnit,
            duration2: Long,
            timeUnit2: TimeUnit,
            duration3: Long,
            timeUnit3: TimeUnit,
            duration4: Long,
            timeUnit4: TimeUnit
              ): TimeDuration = TimeDuration(
            TimeDurationData(
                (finestTimeUnit.convert(duration1, timeUnit1) + finestTimeUnit.convert(duration2, timeUnit2) + finestTimeUnit.convert(
                    duration3,
                    timeUnit3
                                                                                                                                     ) + finestTimeUnit.convert(
                    duration4,
                    timeUnit4
                                                                                                                                                               )),
                finestTimeUnit
                            )
                                            )

        @JvmStatic
        fun of(vararg timeDurations: TimeDuration): TimeDuration = of(timeDurations.asList())
        @JvmStatic
        fun of(timeDurations: Iterable<TimeDuration>): TimeDuration
        {
            var sum: Long = 0
            for (timeDuration in timeDurations)
            {
                sum += timeDuration.toFinestDuration()
            }
            return TimeDuration(TimeDurationData(sum, finestTimeUnit))
        }

        /**
         * Creates a TimeDuration by parsing a case insensitive String describing a time duration string or list of time durations.
         * The time durations can be delimited by a comma, the case insensitive word 'and' (optionally preceded by a
         * comma and a space), or an ampersand (optionally preceded by a comma and a space). The name of any valid
         * [TimeUnit] can be used either in the singular or plural format.
         *
         * Some examples:
         *
         *  * 1 Day
         *  * 2 DAYS
         *  * 2 DAYS, 4 hours, 1 Minute
         *  * 2 days 4 hours 1 Minute
         *  * 2 Days 4 hours and 1 Minute
         *  * 2 Days, 4 hours, and 1 Minute
         *  * 2 Days, and 4 hours, and 1 Minute
         *  * 9 Days, 46 hours, & 5 Minutes
         *  * 5 MINUTES & 30 SECONDS
         *  * 5 Minutes and 30 seconds
         *  * 250 Milliseconds
         *
         * @param text the string to parse
         *
         * @return a TimeDuration with the coarsest possible TimeUnit without truncation
         *
         * @throws NullPointerException      if the supplied string is null, or
         * @throws TimeDurationParseException if the supplied string cannot be parsed
         */
        @Contract("null -> fail")
        @Throws(TimeDurationParseException::class)
        @JvmStatic
        fun of(text: String): TimeDuration = TimeDuration(internalParse(text))
    }
}


// ON THE FENCE AS TO WHETHER WE SHOULD HAVE THESE OR NOT. THEY WOULD MAKE USE A BIT EASIER IN JAVA, BUT THEY CLUTTER THINGS AND DO NOT TEACH THE 'KOTLIN' WAY OF USING CLASSES
// We provide these duplicate "of" factory methods outside of the companion object to make Java use a bit more intuitive.

//fun ofNanos(nanoseconds: Long): TimeDuration = TimeDuration.ofNanos(nanoseconds)
//fun ofMicros(microseconds: Long): TimeDuration = TimeDuration.ofMicros(microseconds)
//fun ofMillis(milliseconds: Long): TimeDuration = TimeDuration.ofMillis(milliseconds)
//fun ofSeconds(seconds: Long): TimeDuration = TimeDuration.ofSeconds(seconds)
//fun ofMinutes(minutes: Long): TimeDuration = TimeDuration.ofMinutes(minutes)
//fun ofHours(hours: Long): TimeDuration = TimeDuration.ofHours(hours)
//fun ofDays(days: Long): TimeDuration = TimeDuration.ofDays(days)
//fun ofSum(timeDurations: Iterable<TimeDuration>) = TimeDuration.ofSum(timeDurations)
//fun ofSum(vararg timeDurations: TimeDuration): TimeDuration = TimeDuration.ofSum(*timeDurations)
//fun of(duration: Long, timeUnit: TimeUnit): TimeDuration = TimeDuration.of(duration, timeUnit)
//fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit): TimeDuration = TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2)
//fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, duration3: Long, timeUnit3: TimeUnit): TimeDuration = TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2, duration3, timeUnit3)
//fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, duration3: Long, timeUnit3: TimeUnit, duration4: Long, timeUnit4: TimeUnit): TimeDuration = TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2, duration3, timeUnit3, duration4, timeUnit4)
//fun of(vararg timeDurations: TimeDuration): TimeDuration = TimeDuration.of(*timeDurations)
//fun of(timeDurations: Iterable<TimeDuration>): TimeDuration = TimeDuration.of(timeDurations)


fun java.time.Duration.toTimeDuration(): TimeDuration = TimeDuration.ofNanos(this.toNanos())
fun org.joda.time.Duration.toTimeDuration(): TimeDuration = TimeDuration.ofMillis(this.millis)
fun org.joda.time.Interval.toTimeDuration(): TimeDuration = TimeDuration.ofMillis(this.toDurationMillis())
fun org.joda.time.Period.toTimeDuration(): TimeDuration = TimeDuration.ofMillis(this.toStandardDuration().millis)
fun TimeUnit.toTimeDuration(duration: Long): TimeDuration = TimeDuration.of(duration, this)


internal val sortedTimeUnits: List<TimeUnit> = createSortedTimeUnitsList()
internal val finestTimeUnit: TimeUnit = sortedTimeUnits[0]


internal fun TimeDuration.toFinestDuration(): Long = finestTimeUnit.convert(this.duration, this.timeUnit)
internal fun toFinestDuration(duration: Long, timeUnit: TimeUnit): Long = finestTimeUnit.convert(duration, timeUnit)


/**
 * Normalizes the supplied TimeDuration by reducing the TimeUnit of the TimeDuration to the coarsest TimeUnit without truncation (i.e. lost of precision).
 * For example:
 * * given a TimeDuration of 24 Hours, 1 Day is returned
 * * given 25 Hours, 25 Hours is returned
 * * given 23 Hours, 23 Hours is returned
 * * given 3,600,000 Milliseconds, 1 Hour is returned
 *
 * @param timeDuration the TimeDuration to normalize
 */
@Contract("null -> null")
fun normalizeToCoarsestTimeUnit(timeDuration: TimeDuration?): TimeDuration? =
    if (timeDuration == null) null else normalizeToCoarsestTimeUnit(timeDuration.duration, timeDuration.timeUnit)

fun normalizeToCoarsestTimeUnit(duration: Long, timeUnit: TimeUnit): TimeDuration =
    TimeDuration(normalizeToCoarsestTimeUnit(TimeDurationData(duration, timeUnit)))

private fun normalizeToCoarsestTimeUnit(data: TimeDurationData): TimeDurationData
{
    // handle the corner case of 0
    if (data.duration == 0L)
    {
        return TimeDurationData(0, finestTimeUnit, true)
    }

    var retval = data
    val startIndex = sortedTimeUnits.indexOf(retval.timeUnit)
    if (startIndex != sortedTimeUnits.size - 1)
    {
        for (i in startIndex + 1 until sortedTimeUnits.size)
        {
            val nextUnit = sortedTimeUnits[i]

            val converted = nextUnit.convert(retval.duration, retval.timeUnit)
            val roundTripped = retval.timeUnit.convert(converted, nextUnit)

            if (roundTripped == retval.duration)
            {
                retval = TimeDurationData(converted, nextUnit)
            }
        }
    }
    return TimeDurationData(retval.duration, retval.timeUnit, true)
}

private fun createSortedTimeUnitsList(): List<TimeUnit>
{
    val timeUnits = TimeUnit.values().toMutableList()
    timeUnits.sortWith(
        Comparator { a, b ->
            // convert 1 B to x A
            // For Example:
            //     1 Day converts to 24 Hours;
            //     1 Hour converts to 60 Minutes;
            //     1 Hour converts to 1 Hour;
            //     1 Hour converts to 0 Seconds
            val x = b.convert(1, a)
            //Logic from Long.compare(long, long), but we use 1 for 'y'
            if (x < 1) -1 else if (x == 1L) 0 else 1
        })
    return timeUnits.toList()
}


private val andRegex = Regex("""(?i),|AND|&|\+""")
private val signRegex = Regex("""(?i)([\-+]?)(.*)""")
private val splittingRegex = Regex("""(?i)[\s]+(?=(([\d]+)([\s]*)([a-z_${'$'}][\w]+)))""")
private val singleTimeDurationParsingRegex = Regex("""(?i)([\d]+)([\s]*)([a-z_${'$'}][\w]+)""")

@Throws(TimeDurationParseException::class)
private fun internalParse(text: String): TimeDurationData
{
    try
    {
        val signResult = signRegex.find(text)
        val isNegative = "-" == signResult?.groups?.get(1)?.value
        val normalized = signResult?.groups?.get(2)?.value?.trim()?.let { andRegex.replace(it, "") }
        val split = normalized?.let { splittingRegex.split(it) }
        var sum = 0L
        split?.forEach {
            sum += parseSingleTimeDurationString(it)
        }
        if (isNegative)
        {
            sum = -sum
        }
        return TimeDurationData(sum, finestTimeUnit)
    }
    catch (e: Exception)
    {
        throw TimeDurationParseException("Could not parse the supplied time string '$text' due to an exception. Cause Summary: $e", e)
    }
}


@Throws(TimeDurationParseException::class)
private fun parseSingleTimeDurationString(text: String): Long
{
    try
    {
        val matchResult = singleTimeDurationParsingRegex.matchEntire(text.trim())
            ?: throw TimeDurationParseException("Supplied string does not represent a valid TimeDuration: '$text'")

        val durationString = matchResult.groups[1]?.value
        var timeUnitString = matchResult.groups[3]?.value?.toUpperCase()

        if (durationString == null || timeUnitString == null)
        {
            throw TimeDurationParseException("Supplied string does not represent a valid TimeDuration. A duration of '$durationString' and a TimeUnit of '$timeUnitString' was parsed out of the text: '$text'")
        }

        if (!timeUnitString.endsWith('S'))
        {
            timeUnitString += "S"
        }
        val timeUnit = TimeUnit.valueOf(timeUnitString)
        val duration = durationString.toLong()
        return toFinestDuration(duration, timeUnit)

    }
    catch (e: TimeDurationParseException)
    {
        throw e
    }
    catch (e: Exception)
    {
        throw TimeDurationParseException("Could not parse the supplied time string '$text' due to an exception. Cause Summary: $e", e)
    }
}

internal data class TimeDurationData(val duration: Long, val timeUnit: TimeUnit = TimeUnit.MILLISECONDS, val hasBeenNormalized: Boolean = false) :
    Comparable<TimeDurationData>
{
    private val finestValue = toFinestDuration(duration, timeUnit)
    override fun compareTo(other: TimeDurationData): Int = finestValue.compareTo(other.finestValue)
}


internal object TimeDurationExampleKotlin
{
    /** Examples of creating TimeDurations in Kotlin. */
    @Suppress("UNUSED_VARIABLE")
    private fun creatingTimeDurationsInKotlinExample()
    {

        // via factory methods
        val timeDuration1 = TimeDuration.of(5, TimeUnit.MINUTES)
        val timeDuration2 = TimeDuration.of(500, TimeUnit.MILLISECONDS)

        // via cumulative summing factory method. This example ultimately creates a
        // TimeDuration of 90 Seconds, but it can be more intuitive expressed this way
        // or useful in some situations
        val timeDuration3 = TimeDuration.of(1, TimeUnit.MINUTES, 30, TimeUnit.SECONDS)

        //from a String
        val timeDuration4 = TimeDuration.of("5 Minutes")
        val timeDuration5 = TimeDuration.of("5 minutes and 30 seconds")

        // via a ofUnit factory method
        val timeDuration6 = TimeDuration.ofSeconds(15)
        val timeDuration7 = TimeDuration.ofMillis(500)
    }

}
