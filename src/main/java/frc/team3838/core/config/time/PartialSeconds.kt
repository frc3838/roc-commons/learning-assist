package frc.team3838.core.config.time


import frc.team3838.core.config.time.TimeDuration.Companion.ofMillis
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.ToLongFunction

@Suppress("unused")
enum class PartialSeconds
{
    /** No partial value (i.e. 0 milliseconds).  */
    Zero
    {
        override fun toMills(): Long = 0
    },

    /** 1/16 of a second (63 milliseconds).  */
    OneSixteenthSecond
    {
        override fun toMills(): Long = 63
    },

    /** 1/10 of a second (100 milliseconds).  */
    OneTenthSecond
    {
        override fun toMills(): Long = 100
    },

    /** 1/8 of a second (125 milliseconds).  */
    OneEighthSecond
    {
        override fun toMills(): Long = 125
    },

    /** 3/16 of a second (188 milliseconds).  */
    ThreeSixteenthSecond
    {
        override fun toMills(): Long = 188
    },

    /** 2/10 of a second (200 milliseconds).  */
    TwoTenthSecond
    {
        override fun toMills(): Long = 200
    },

    /** 1/4 of a second (250 milliseconds).  */
    OneQuarterSecond
    {
        override fun toMills(): Long = 250
    },

    /** 3/10 of a second (300 milliseconds).  */
    ThreeTenthSecond
    {
        override fun toMills(): Long = 300
    },

    /** 5/16 of a second (313 milliseconds).  */
    FiveSixteenthSecond
    {
        override fun toMills(): Long = 313
    },

    /** 3/8 of a second (375 milliseconds).  */
    ThreeEightsSecond
    {
        override fun toMills(): Long = 375
    },

    /** 4/10 of a second (400 milliseconds).  */
    FourTenthSecond
    {
        override fun toMills(): Long = 400
    },

    /** 7/16 of a second (438 milliseconds).  */
    SevenSixteenthSecond
    {
        override fun toMills(): Long = 438
    },

    /** 1/2 of a second (500 milliseconds).  */
    HalfSecond
    {
        override fun toMills(): Long = 500
    },

    /** 9/16 of a second (563 milliseconds).  */
    NineSixteenthSecond
    {
        override fun toMills(): Long = 563
    },

    /** 6/10 of a second (600 milliseconds).  */
    SixTenthSecond
    {
        override fun toMills(): Long = 600
    },

    /** 5/8 of a second (625 milliseconds).  */
    FiveEightsSecond
    {
        override fun toMills(): Long = 625
    },

    /** 11/16 of a second (688 milliseconds).  */
    ElevenSixteenthSecond
    {
        override fun toMills(): Long = 688
    },

    /** 7/10 of a second (700 milliseconds).  */
    SevenTenthSecond
    {
        override fun toMills(): Long = 700
    },

    /** 3/4 of a second (750 milliseconds).  */
    ThreeQuartersSecond
    {
        override fun toMills(): Long = 750
    },

    /** 8/10 of a second (800 milliseconds).  */
    EightTenthSecond
    {
        override fun toMills(): Long = 800
    },

    /** 13/16 of a second (813 milliseconds).  */
    ThirteenSixteenthSecond
    {
        override fun toMills(): Long = 813
    },

    /** 7/8 of a second (875 milliseconds).  */
    SevenEightsSecond
    {
        override fun toMills(): Long = 875
    },

    /** 9/10 of a second (900 milliseconds).  */
    nineTenthSecond
    {
        override fun toMills(): Long = 900
    },

    /** 15/16 of a second (938 milliseconds).  */
    FifteenSixteenthSecond
    {
        override fun toMills(): Long = 938
    },

    /**  A full second (1,000 milliseconds).  */
    FullSecond
    {
        override fun toMills(): Long = 1000
    };

    abstract fun toMills(): Long

    fun toTimeDuration(): TimeDuration = ofMillis(toMills())

    companion object
    {
        fun sumToMillis(partialSeconds: PartialSeconds): Long = sumToMillis(0, partialSeconds)

        fun sumToMillis(seconds: Long, vararg partialSeconds: PartialSeconds): Long =
            TimeUnit.SECONDS.toMillis(seconds) + Arrays.stream(partialSeconds).mapToLong(ToLongFunction { obj: PartialSeconds -> obj.toMills() }).sum()
    }
}