package frc.team3838.core.config.time

@Suppress("unused")
class TimeDurationParseException : RuntimeException
{
    constructor(message: String) : super(message)


    constructor(message: String, cause: Throwable) : super(message, cause)


    constructor(message: String, cause: Throwable, enableSuppression: Boolean, writableStackTrace: Boolean) : super(
        message,
        cause,
        enableSuppression,
        writableStackTrace
                                                                                                                   )

    companion object
    {
        private const val serialVersionUID = -2434943932112081705L
    }
}
