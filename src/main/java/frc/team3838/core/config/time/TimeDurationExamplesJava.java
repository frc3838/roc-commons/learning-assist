package frc.team3838.core.config.time;


import java.util.concurrent.TimeUnit;



@SuppressWarnings("unused")
class TimeDurationExamplesJava
{

static void creatingTimeDurationsInJava()
{
    // Because the TimeDuration class, written in Kotlin, has its "of" factory functions
    // contained within its Companion object -- the places where a Kotlin class put
    // functions that are equivalent to static methods in Java -- you access them
    // via the Companion object
    
    // via factory methods
    TimeDuration timeDuration1 = TimeDuration.Companion.of(5, TimeUnit.MINUTES);
    TimeDuration timeDuration2 = TimeDuration.Companion.of(500, TimeUnit.MILLISECONDS);

    // via cumulative summing factory method. This example ultimately creates a
    // TimeDuration.Companion.of 90 Seconds, but it can be more intuitive expressed this way
    // or useful in some situations
    TimeDuration timeDuration3 = TimeDuration.Companion.of(1, TimeUnit.MINUTES, 30, TimeUnit.SECONDS);

    //from a String
    TimeDuration timeDuration4 = TimeDuration.Companion.of("5 Minutes");
    TimeDuration timeDuration5 = TimeDuration.Companion.of("5 minutes and 30 seconds");

    // via a ofUnit factory method
    TimeDuration timeDuration6 = TimeDuration.Companion.ofSeconds(15);
    TimeDuration timeDuration7 = TimeDuration.Companion.ofMillis(500);
    
    
}

}
