package frc.team3838.core.config.time

import java.util.*
import java.util.concurrent.TimeUnit

@Suppress("unused")
enum class PartialMinutes
{
    NoPartial
    {
        override fun toMills(): Long = 0
    },
    OneSixteenthMinute
    {
        override fun toMills(): Long = 3750
    },
    OneEighthMinute
    {
        override fun toMills(): Long = 7500
    },
    ThreeSixteenthMinute
    {
        override fun toMills(): Long = 11250
    },
    OneQuarterMinute
    {
        override fun toMills(): Long = 15000
    },
    FiveSixteenthMinute
    {
        override fun toMills(): Long = 18750
    },
    ThreeEightsMinute
    {
        override fun toMills(): Long = 18750
    },
    SevenSixteenthMinute
    {
        override fun toMills(): Long = 26250
    },
    HalfMinute
    {
        override fun toMills(): Long = 30000
    },
    NineSixteenthMinute
    {
        override fun toMills(): Long = 33750
    },
    FiveEightsMinute
    {
        override fun toMills(): Long = 37500
    },
    ElevenSixteenthMinute
    {
        override fun toMills(): Long = 41250
    },
    ThreeQuartersMinute
    {
        override fun toMills(): Long = 45000
    },
    ThirteenSixteenthMinute
    {
        override fun toMills(): Long = 48750
    },
    SevenEightsMinute
    {
        override fun toMills(): Long = 52500
    },
    FifteenSixteenthMinute
    {
        override fun toMills(): Long = 56250
    },
    FullMinute
    {
        override fun toMills(): Long = TimeUnit.MINUTES.toMillis(1)
    };

    abstract fun toMills(): Long

    companion object
    {
        fun sumToMillis(minutes: Long, vararg partialMinutes: PartialMinutes): Long
        {
            return TimeUnit.MINUTES.toMillis(minutes) + Arrays.stream(partialMinutes).mapToLong { it.toMills()}.sum()
        }
    }
}