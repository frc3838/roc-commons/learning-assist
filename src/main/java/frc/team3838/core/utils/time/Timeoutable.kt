package frc.team3838.core.utils.time


interface Timeoutable
{
    fun hasTimedOut(): Boolean
}