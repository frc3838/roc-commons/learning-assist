package frc.team3838.core.utils.time

import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import frc.team3838.core.utils.format
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * A timer used to determine if an operation has timed out. Use the factory methods to create:
 * @sample TimeOutTimerSamples.factoryMethodSamples
 */
interface TimeoutTimer : Timeoutable
{
    /**
     * If there is a timeout set or not. Allows for the use of a Timeout as an optional property value.
     * Use the [createNoTimeout] factory method to get a TimeoutTimer for no timeout.
     */
    val hasNoTimeoutSet: Boolean

    /**
     * Resets and starts the timer. The function returns itself for use in a chained assignment such as:
     * `val timer = Timer(timeDuration).start()`
     */
    fun start(): TimeoutTimer

    /** If the timer has timed out. */
    override fun hasTimedOut(): Boolean

    /** Returns the time remaining in milliseconds. */
    @API
    fun getTimeRemainingInMs(): Long

    /**
     * Returns a string indicating the time remaining. Example output:
     *  - 5,341ms
     *  - 142ms
     *  - Has Timed Out
     *  - No Timeout
     *  @see getTimeRemainingFormatted
     */
    @API
    fun getTimeRemaining(): String

    /**
     * Returns a string indicating the time remaining, in milliseconds. Example output:
     *  - 2 minutes, 1 second and 250 milliseconds
     *  - 5 seconds
     *  - Has Timed Out
     *  - "No Timeout Set"
     *  @see getTimeRemaining
     */
    fun getTimeRemainingFormatted(): String

    /**
     * Returns the time remaining as a [TimeDuration]
     */
    fun getTimeDurationRemaining(): TimeDuration

    companion object
    {
        @API
        @JvmStatic
        val NO_TIMEOUT = TimeDuration.of(Long.MAX_VALUE, TimeUnit.MILLISECONDS)

        @API
        @JvmStatic
        fun createNoTimeout(): TimeoutTimer = NoTimeoutTimeoutTimer

        @API
        @JvmStatic
        fun create(timeDuration: TimeDuration?): TimeoutTimer = TimeoutTimerImpl(timeDuration)

        @API
        @JvmStatic
        fun createForSeconds(timeDurationInSeconds: Int?): TimeoutTimer = if (timeDurationInSeconds == null) NoTimeoutTimeoutTimer else  TimeoutTimerImpl(TimeDuration.ofSeconds(timeDurationInSeconds.toLong()))

        @API
        @JvmStatic
        fun createForSeconds(timeDurationInSeconds: Double?): TimeoutTimer = if (timeDurationInSeconds == null) NoTimeoutTimeoutTimer else  TimeoutTimerImpl(TimeDuration.ofSeconds(timeDurationInSeconds))

        @API
        @JvmStatic
        fun createForSecondsAndMs(seconds: Int, milliseconds: Int): TimeoutTimer = TimeoutTimerImpl(TimeDuration.ofSecondsAndMs(seconds.toLong(), milliseconds.toLong()))

    }
}

@API


private class TimeoutTimerImpl(timeDuration: TimeDuration?) : TimeoutTimer by if (timeDuration == null || timeDuration == TimeoutTimer.NO_TIMEOUT || timeDuration.duration == Long.MAX_VALUE || timeDuration.duration <= 0) NoTimeoutTimeoutTimer else TimeoutTimerInternal(timeDuration)

private object NoTimeoutTimeoutTimer : TimeoutTimer
{
    override val hasNoTimeoutSet: Boolean = true

    override fun start(): TimeoutTimer = this

    override fun hasTimedOut(): Boolean = false

    override fun getTimeRemainingInMs(): Long = Long.MAX_VALUE

    override fun getTimeRemaining(): String = "No Timeout"

    override fun getTimeRemainingFormatted(): String = "No Timeout"

    override fun getTimeDurationRemaining(): TimeDuration = TimeoutTimer.NO_TIMEOUT
}

private class TimeoutTimerInternal(val timeDuration: TimeDuration) : TimeoutTimer
{
    private val timeoutDurationInMs = timeDuration.toMillis()
    private var timeoutTime: Long = 0
    private var hasTimedOut = false
    private val lock = ReentrantReadWriteLock()


    override val hasNoTimeoutSet: Boolean = false

    override fun start(): TimeoutTimer
    {
        lock.write {
            hasTimedOut = false
            timeoutTime = getFpgaTimeInMilliseconds() + timeoutDurationInMs
        }
        return this
    }

    override fun hasTimedOut(): Boolean
    {
        lock.read {
            if (hasTimedOut) return true
        }

        lock.write {
            hasTimedOut = getFpgaTimeInMilliseconds() >= timeoutTime
        }
        return hasTimedOut
    }

    override fun getTimeRemainingInMs(): Long = timeoutTime - getFpgaTimeInMilliseconds()


    override fun getTimeRemaining(): String
    {
        val timeLeft = getTimeRemainingInMs()
        return if (timeLeft < 0)
        {
            "Has Timed Out"
        }
        else
        {
            "${timeLeft.format(0)}ms"
        }
    }

    override fun getTimeRemainingFormatted(): String
    {
        val timeLeft = getTimeRemainingInMs()
        return if (timeLeft < 0)
        {
            "Has Timed Out"
        }
        else
        {
            TimeDuration.ofMillis(timeLeft).toStringFormatted()
        }
    }

    override fun getTimeDurationRemaining(): TimeDuration = TimeDuration.ofMillis(getTimeRemainingInMs())
}

private object TimeOutTimerSamples
{
    fun factoryMethodSamples()
    {

        TimeoutTimer.createNoTimeout()

        TimeoutTimer.create(TimeDuration.of("3 seconds and 250 milliseconds"))
        TimeoutTimer.create(TimeDuration.ofSeconds(3))
        TimeoutTimer.create(TimeDuration.ofSeconds(3.25))
        TimeoutTimer.create(TimeDuration.ofSecondsAndMs(3, 250))

        TimeoutTimer.createForSeconds(3)
        TimeoutTimer.createForSeconds(3.25)
        TimeoutTimer.createForSecondsAndMs(3, 250)


        // To create a started Timeout timer, simply add a call to the
        // start() method, which returns the TimeoutTimer instance

        TimeoutTimer.create(TimeDuration.of("3 seconds and 250 milliseconds")).start()
        TimeoutTimer.create(TimeDuration.ofSeconds(3)).start()
        TimeoutTimer.create(TimeDuration.ofSeconds(3.25)).start()
        TimeoutTimer.create(TimeDuration.ofSecondsAndMs(3, 250)).start()

        TimeoutTimer.createForSeconds(3).start()
        TimeoutTimer.createForSeconds(3.25).start()
        TimeoutTimer.createForSecondsAndMs(3, 250).start()
    }
}

