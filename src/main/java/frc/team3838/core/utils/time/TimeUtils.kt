package frc.team3838.core.utils.time

import edu.wpi.first.wpilibj.RobotController
import frc.team3838.core.meta.API
import org.joda.time.Duration
import org.joda.time.format.PeriodFormat
import java.util.concurrent.TimeUnit


/**
 * Return the system clock time in milliseconds. Return the time from the FPGA hardware clock in
 * milliseconds since the FPGA started.
 *
 * @return Robot running time in milliseconds.
 */
@API
fun getFpgaTimeInMilliseconds(): Long = RobotController.getFPGATime() / 1_000

/**
 * Return the system clock time in seconds. Return the time from the FPGA hardware clock in
 * seconds since the FPGA started.
 *
 * @return Robot running time in seconds.
 */
@API
fun getFpgaTimeInSeconds(): Double = RobotController.getFPGATime() / 1_000_000.0

/**
 * Returns a formatted String for a duration such as "76 hours, 18 minutes, 1 second and 250 milliseconds".
 */
fun Long.millsToFormattedDuration(): String = PeriodFormat.getDefault().print(Duration.millis(this).toPeriod())
fun sleep(time: Long, timeUnit: TimeUnit = TimeUnit.MILLISECONDS)
{
    try { timeUnit.sleep(time) } catch (ignore: InterruptedException) { }
}

fun sleepMs(time: Long)
{
    sleep(time, TimeUnit.MILLISECONDS)
}

fun sleepSeconds(time: Long)
{
    sleep(time, TimeUnit.SECONDS)
}