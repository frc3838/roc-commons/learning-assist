@file:JvmName("MathUtils")
@file:Suppress("FunctionName", "unused")

package frc.team3838.core.utils

import mu.KotlinLogging
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.math.pow
import kotlin.math.roundToInt

private val logger = KotlinLogging.logger {}

val DECIMAL_FORMATTER_0_PLACES = DecimalFormat("#,##0")
val DECIMAL_FORMATTER_1_PLACES = DecimalFormat("#,##0.0")
val DECIMAL_FORMATTER_2_PLACES = DecimalFormat("#,##0.00")
val DECIMAL_FORMATTER_3_PLACES = DecimalFormat("#,##0.000")
val DECIMAL_FORMATTER_4_PLACES = DecimalFormat("#,##0.0000")
val DECIMAL_FORMATTER_5_PLACES = DecimalFormat("#,##0.00000")
val DECIMAL_FORMATTER_6_PLACES = DecimalFormat("#,##0.000000")

@JvmOverloads
fun Number.format(places: Int = 2): String = selectDecimalFormatter(places).format(this)

fun Number.toString(places: Int): String = selectDecimalFormatter(places).format(this)

@JvmOverloads
fun selectDecimalFormatter(places: Int = 2): DecimalFormat
{
    return when (places)
    {
        0 -> DECIMAL_FORMATTER_0_PLACES
        1 -> DECIMAL_FORMATTER_1_PLACES
        2 -> DECIMAL_FORMATTER_2_PLACES
        3 -> DECIMAL_FORMATTER_3_PLACES
        4 -> DECIMAL_FORMATTER_4_PLACES
        5 -> DECIMAL_FORMATTER_5_PLACES
        else -> DECIMAL_FORMATTER_6_PLACES
    }
}

fun Float.makeNegative(): Float = -abs(this)
fun Float.makePositive(): Float = abs(this)
fun Double.makeNegative(): Double = -abs(this)
fun Double.makePositive(): Double = abs(this)
fun Int.makeNegative(): Int = -abs(this)
fun Int.makePositive(): Int = abs(this)
fun Long.makeNegative(): Long = -abs(this)
fun Long.makePositive(): Long = abs(this)

fun Float.isPositive(): Boolean = this >= 0.0
fun Float.isNegative(): Boolean = this < 0.0
fun Double.isPositive(): Boolean = this >= 0.0
fun Double.isNegative(): Boolean = this < 0.0
fun Int.isPositive(): Boolean = this >= 0
fun Int.isNegative(): Boolean = this < 0
fun Long.isPositive(): Boolean = this >= 0
fun Long.isNegative(): Boolean = this < 0

/** Returns the RECEIVER VALUE with the same sign as the valueToMatch. */
fun Float.matchSignTo(valueToMatch: Float): Float = if (valueToMatch.isPositive()) this.makePositive()  else this.makeNegative()

/** Returns the RECEIVER VALUE with the same sign as the valueToMatch. */
fun Double.matchSignTo(valueToMatch: Double): Double = if (valueToMatch.isPositive()) this.makePositive()  else this.makeNegative()

/** Returns the RECEIVER VALUE with the same sign as the valueToMatch. */
fun Int.matchSignTo(valueToMatch: Int): Int = if (valueToMatch.isPositive()) this.makePositive()  else this.makeNegative()

/** Returns the RECEIVER VALUE with the same sign as the valueToMatch. */
fun Long.matchSignTo(valueToMatch: Long): Long = if (valueToMatch.isPositive()) this.makePositive()  else this.makeNegative()


/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Float.hasSameSignAs(other: Float): Boolean = this == 0.0F || other == 0.0F || this > 0 && other > 0 || this < 0 && other < 0


/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Float.hasSameSignAs(other: Int): Boolean = this == 0.0F || other == 0 || this > 0 && other > 0 || this < 0 && other < 0

/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Double.hasSameSignAs(other: Double): Boolean = this == 0.0 || other == 0.0 || this > 0 && other > 0 || this < 0 && other < 0

/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Double.hasSameSignAs(other: Int): Boolean = this == 0.0 || other == 0 || this > 0 && other > 0 || this < 0 && other < 0

/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Int.hasSameSignAs(other: Int): Boolean = this == 0 || other == 0 || this > 0 && other > 0 || this < 0 && other < 0

/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Int.hasSameSignAs(other: Double): Boolean = this == 0 || other == 0.0 || this > 0 && other > 0 || this < 0 && other < 0

/**
 * Determines if a value (the receiver) has the same sign as another value.
 * If either value is zero, the method will return true.
 *
 * @return true if a both values are the same sign, or if one (or both) values is zero; false otherwise.
 */
fun Long.hasSameSignAs(other: Long): Boolean = this == 0L || other == 0L || this > 0 && other > 0 || this < 0 && other < 0


/**
 * Performs an operation on the absolute value of a number, then returns the result of the
 * operation with the same sign as the the original number. For example, if you want to
 * increase a speed value by a relative 0.1 such that a forward speed of 0.5 returns 0.6 and
 * an reverse speed of -0.5 returns -0.6:
 *
 * ```
 * // Kotlin
 * val modifiedSpeed = speed.operateOnAbsValue {
 *     it + 0.1
 * }
 * ```
 *
 * @receiver the number to perform the operation on
 * @param action the action to perform
 */
fun Float.operateOnAbsValue(action: (Float) -> Float): Float
{
    val isNeg = this.isNegative()
    val result = action(this.absoluteValue)
    return if (isNeg) -result else result
}

/**
 * Performs an operation on the absolute value of a number, then returns the result of the
 * operation with the same sign as the the original number. For example, if you want to
 * increase a speed value by a relative 0.1 such that a forward speed of 0.5 returns 0.6 and
 * an reverse speed of -0.5 returns -0.6:
 *
 * ```
 * // Kotlin
 * val modifiedSpeed = speed.operateOnAbsValue {
 *     it + 0.1
 * }
 * ```
 *
 * @receiver the number to perform the operation on
 * @param action the action to perform
 */
fun Double.operateOnAbsValue(action: (Double) -> Double): Double
{
    val isNeg = this.isNegative()
    val result = action(this.absoluteValue)
    return if (isNeg) -result else result
}

/**
 * Performs an operation on the absolute value of a number, then returns the result of the
 * operation with the same sign as the the original number. For example, if you want to
 * increase a speed value by a relative 1 such that a forward speed of 5 returns 6 and
 * an reverse speed of -5 returns -6:
 *
 * ```
 * // Kotlin
 * val modifiedSpeed = speed.operateOnAbsValue {
 *     it + 1
 * }
 * ```
 *
 * @receiver the number to perform the operation on
 * @param action the action to perform
 */
fun Int.operateOnAbsValue(action: (Int) -> Int): Int
{
    val isNeg = this.isNegative()
    val result = action(this.absoluteValue)
    return if (isNeg) -result else result
}

/**
 * Performs an operation on the absolute value of a number, then returns the result of the
 * operation with the same sign as the the original number. For example, if you want to
 * increase a speed value by a relative 1 such that a forward speed of 5 returns 6 and
 * an reverse speed of -5 returns -6:
 *
 * ```
 * // Kotlin
 * val modifiedSpeed = speed.operateOnAbsValue {
 *     it + 1L
 * }
 * ```
 *
 * @receiver the number to perform the operation on
 * @param action the action to perform
 */
fun Long.operateOnAbsValue(action: (Long) -> Long): Long
{
    val isNeg = this.isNegative()
    val result = action(this.absoluteValue)
    return if (isNeg) -result else result
}


/**
 * Scales a value from one range of numbers to another range of numbers. 
 * For example, to scale a joystick axis input of `-1.0 to 1.0` to a PWM raw value
 * range of `0 to 255`, usage would be:
 * ```
 * // Java
 * double out = MathUtils.scaleRange(joystick.getY(), -1, 1, 0, 255);
 * // Kotlin
 * val out = joystick.getY().scaleRange(-1.0, 1.0, 0.0, 255.0)
 * ```
 *
 * Also see various the various `scaleXyz(double)` methods that do common scaling conversions.
 *
 * @receiver the input value to scale to new range
 * @param inputRangeMin  the minimum value of the input range
 * @param inputRangeMax  the maximum value of the input range
 * @param outputRangeMin the minimum value of the output range
 * @param outputRangeMax the maximum value of the output range
 *
 * @return the scaled value
 */
fun Double.scaleRange(inputRangeMin: Double, inputRangeMax: Double,
                      outputRangeMin: Double, outputRangeMax: Double): Double
{
    //http://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
    return (outputRangeMax - outputRangeMin) * (this - inputRangeMin) / (inputRangeMax - inputRangeMin) + outputRangeMin
}

/**
 * Scales a value from one range of numbers to another range of numbers.
 * For example, to scale a joystick axis input of `-1.0 to 1.0` to a PWM raw value
 * range of `0 to 255`, usage would be:
 * ```
 * // Java
 * double out = MathUtils.scaleRange(joystick.getY(), -1, 1, 0, 255);
 * // Kotlin
 * val out = joystick.getY().scaleRange(-1.0, 1.0, 0, 255)
 * ```
 *
 * Also see various the various `scaleXyz(double)` methods that do common scaling conversions.
 *
 * @receiver the input value to scale to new range
 * @param inputRangeMin  the minimum value of the input range
 * @param inputRangeMax  the maximum value of the input range
 * @param outputRangeMin the minimum value of the output range
 * @param outputRangeMax the maximum value of the output range
 *
 * @return the scaled value
 */
fun Double.scaleRangeToInt(inputRangeMin: Double, inputRangeMax: Double,
                           outputRangeMin: Int, outputRangeMax: Int): Int = this.scaleRange(inputRangeMin, inputRangeMax, outputRangeMin.toDouble(), outputRangeMax.toDouble()).toInt()




/**
 * Scales a -1 to 1 speed range to remove stall speeds. For example, if the robot does not start moving
 * until the power speed is set to 0.3, we have a stall speed of 0.3. This method will scale the
 * the speed from 0.3 to 1 and -0.3 to -1 (with a speed od 0 returning zero).
 *
 * @receiver          the speed value read from the joystick or otherwise calculated.
 * @param stallSpeed  the absolute (i.e. positive) value of the stall speed
 *
 * @return the scaled speed in the range `stallSpeed` to 1, `-stallSpeed` to -1, or 0
 */
fun Double.scaleP1toN1SpeedToNonStallRange(stallSpeed: Double): Double
{
    if (this == 0.0) return 0.0

    return this.operateOnAbsValue {
        it.scaleRange(0.0, 1.0, stallSpeed.absoluteValue, 1.0)
    }
}


/**
 * Scales a -1 to 1 speed range to remove stall speeds, and compensate for joystick drift.
 * For example, if the robot does not start moving
 * until the power speed is set to 0.3, we have a stall speed of 0.3. This method will scale the
 * the speed from 0.3 to 1 and -0.3 to -1. If the input value is in the range of
 * `-joystickDriftCutOffValue` to `joystickDriftCutOffValue` a value of zero is returned.
 *
 * @receiver                       the speed value read from the joystick or otherwise calculated.
 * @param stallSpeed               the absolute (i.e. positive) value of the stall speed'
 * @param joystickDriftCutOffValue the absolute (i.e. positive) joystick drift value.
 *
 * @return the scaled speed in the range `stallSpeed` to 1, `-stallSpeed` to -1, or 0
 */
fun Double.scaleP1toN1SpeedToNonStallRange(stallSpeed: Double, joystickDriftCutOffValue: Double): Double
{
    if (this.absoluteValue <= joystickDriftCutOffValue.absoluteValue) return 0.0

    return this.operateOnAbsValue {
        it.scaleRange(0.0, 1.0, stallSpeed.absoluteValue, 1.0)
    }
}





/**
 * Scales an input value of `-1 to 1` to an int value of `0 to 255`.
 *
 * @receiver the value to scale
 *
 * @return the scaled value
 */
fun Double.scaleN1TP1_to_0T255(): Int = scaleRangeToInt(-1.0, 1.0, 0, 255)


/**
 * Scales an input value of `-1 to 1` to an int value of `-255 to 255`.
 *
 * @receiver the value to scale
 *
 * @return the scaled value
 */
fun Double.scaleN1TP1_to_N255T255(): Int = scaleRangeToInt(-1.0, 1.0, -255, 255)


/**
 * Scales an input value of `0 to 255` to a value of `-1 to 1`.
 *
 * @receiver the value to scale
 *
 * @return the scaled value
 */
fun Double.scale0T255_to_N1TP1(): Double = scaleRange(0.0, 255.0, -1.0, 1.0)


/**
 * Scales an input value of `-255 to 255` to a value of `-1 to 1`.
 *
 * @receiver the value to scale
 *
 * @return the scaled value
 */
fun Double.scaleN255T255_to_N1TP1(): Double = scaleRange(-255.0, 255.0, -1.0, 1.0)

/**
 * Converts a decimal to a percentage String (including the '%' sign) with no decimal places and no rounding.
 *
 * @receiver the value to convert to a percentage
 *
 * @return the value as a percentage String
 */
fun Double.toPercentage(): String = this.toPercentage(0, false)


/**
 * Converts a decimal number to a percentage String (including the '%' sign).
 *
 * @receiver        the value to convert to a percentage
 * @param precision the number of decimal places to have in the percentage value; must be between 0 and 5 inclusive
 * @param round     if true, the value will be rounded up, otherwise irt will be floored
 *
 * @return the value as a percentage String
 */
fun Double.toPercentage(precision: Int, round: Boolean): String
{
    var ourPrecision = precision
    val roundFactor = if (round) 0.5 else 0.0
    return if (ourPrecision <= 0)
    {
        "${floor(this * 100 + roundFactor).toLong()}%"
    }
    else
    {
        // technically we should make sure that the value * the shiftFactor does not exceed
        // Double.MAX_VALUE.  But that is an unlikely use case, so we favor performance over safety
        ourPrecision = 5.coerceAtMost(ourPrecision)
        val shiftFactor = 10.0.pow(ourPrecision.toDouble())
        val shifted = floor(this * 100 * shiftFactor + roundFactor).toLong()
        val result = shifted / shiftFactor
        "$result%"
    }
}


fun Double.constrainBetweenZeroAndOne(): Double
{
    return when
    {
        this > 1.0 -> 1.0
        this < 0.0 -> 0.0
        else    -> this
    }
}


fun Double.constrainBetweenNegOneAndZero(): Double
{
    return when
    {
        this > 0.0  -> 0.0
        this < -1.0 -> -1.0
        else     -> this
    }
}


fun Double.constrainBetweenNegOneAndOne(): Double
{
    return when
    {
        this > 1.0  -> 1.0
        this < -1.0 -> -1.0
        else     -> this
    }
}


fun Int.constrainBetweenZeroAnd360(): Int
{
    return when
    {
        this > 360 -> 360
        this < 0   -> 0
        else        -> this
    }
}


fun Double.constrainBetweenZeroAnd360(): Double
{
    return when
    {
        this > 360.0 -> 360.0
        this < 0.0   -> 0.0
        else          -> this
    }
}


fun Int.constrain(min: Int, max: Int): Int
{
    return when
    {
        this > max -> max
        this < min -> min
        else        -> this
    }
}


fun Double.constrain(min: Int, max: Int): Double
{
    return when
    {
        this > max -> max.toDouble()
        this < min -> min.toDouble()
        else        -> this
    }
}

fun Double.constrain(min: Double, max: Double): Double
{
    return when
    {
        this > max -> max
        this < min -> min
        else       -> this
    }
}

fun Float.constrain(min: Int, max: Int): Float
{
    return when
    {
        this > max -> max.toFloat()
        this < min -> min.toFloat()
        else        -> this
    }
}


fun Float.constrain(min: Float, max: Float): Float
{
    return when
    {
        this > max -> max
        this < min -> min
        else        -> this
    }
}




/**
 * Constrains the absolute value to a range of 0 to 1, retaining the sign when returned.
 * For example, if a value of -2 is received, a value of -1 is returned.
 *
 * @receiver The value to constrain
 *
 * @return the constrained value value
 */
fun Double.constrainAbsoluteValueToOne(): Double
{
    return operateOnAbsValue {
        constrainBetweenZeroAndOne()
    }

}

/**
 * Constrains the absolute value to a range, retaining the sign when returned. For example, if a value
 * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Max value
 * should be positive value.
 *
 * @receiver    The value to constrain
 * @param max   The inclusive absolute max value; must be greater than zero
 *
 * @return the constrained value value
 */
fun Double.constrainAbsoluteMaxValue(max: Int): Double
{
    require(max >= 0) { "Max value passed into 'Double.constrainAbsoluteMaxValue(Int)' method must be positive but was $max" }
    return operateOnAbsValue {
        constrain(0, max)
    }
}

/**
 * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
 * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Max value
 * should be positive value.
 *
 * @receiver   The value to constrain
 * @param max  The inclusive absolute max value; must be greater than zero
 *
 * @return the constrained value value
 */
fun Double.constrainAbsoluteMaxValue(max: Double): Double
{
    require(max >= 0) { "Max value passed into 'Double.constrainAbsoluteMaxValue(Double)' method must be positive but was $max" }
    return operateOnAbsValue {
        constrain(0.0, max)
    }
}


/**
 * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
 * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Max value
 * should be positive value.
 *
 * @receiver   The value to constrain
 * @param max  The inclusive absolute max value; must be greater than zero
 *
 * @return the constrained value value
 */
fun Float.constrainAbsoluteMaxValue(max: Float): Float
{
    require(max >= 0) { "Max value passed into 'Float.constrainAbsoluteMaxValue(Float)' method must be positive but was $max" }
    return operateOnAbsValue {
        constrain(0.0F, max)
    }
}


/**
 * Constrains the absolute  value to a range, retaining the sign when returned. For example, if a value
 * of -2 is received with a constraint of 0 to 1, a value of -1 is returned. Passed in Max value
 * should be positive value.
 *
 * @receiver   The value to constrain
 * @param max  The inclusive absolute max value; must be greater than zero
 *
 * @return the constrained value value
 */
fun Float.constrainAbsoluteMaxValue(max: Int): Float
{
    require(max >= 0) { "Max value passed into 'Float.constrainAbsoluteMaxValue(Int)' method must be positive but was $max" }
    return operateOnAbsValue {
        constrain(0, max)
    }
}


fun Double.roundToNPlaces(n: Int): Double = BigDecimal(this, MathContext.DECIMAL128).setScale(n, RoundingMode.HALF_EVEN).toDouble()


fun Float.roundToNPlaces(n: Int): Float = BigDecimal(toDouble(), MathContext.DECIMAL128).setScale(n, RoundingMode.HALF_EVEN).toFloat()


/**
 * Floors a double value to the nearest (lower) X percent. For example, for x = 5, which would
 * give 5% increments (i.e. 0%, 5%, 10%, 15%, etc), this will evaluate as:
 *  * 0.025 = 0.0
 *  * 0.049 = 0.0
 *  * 0.05  = 0.05
 *  * 0.051 = 0.05
 *  * 0.144 = 0.1
 *  * 0.149 = 0.1
 *  * 0.15 = 0.15
 *  * 0.151 = 0.15
 *  * 0.394 = 0.35
 *
 * @receiver the number to floor
 * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
 *
 * @return the floored value
 */
fun Double.floorToXPercent(x: Int): Double
{
    val percent = this * 100
    val v = percent - percent % x
    return v / 100
}

/**
 * Rounds a double value up or down to the nearest X percent. For example, for x = 5, which would
 * give 5% increments (i.e. 0%, 5%, 10%, 15%, etc), this will evaluate as:
 *  * 0.025 = 0.05
 *  * 0.049 = 0.05
 *  * 0.05 = 0.05
 *  * 0.051 = 0.05
 *  * 0.144 = 0.15
 *  * 0.149 = 0.15
 *  * 0.15 = 0.15
 *  * 0.151 = 0.15
 *  * 0.394 = 0.4
 *
 * @receiver the number to round
 * @param x the target percentage increments. For example, for 5% (i.e. 0.05) it would be 5.
 *
 * @return the rounded value
 */
fun Double.roundToXPercent(x: Int): Double
{
    val percent = this * 100
    val q = (percent / x).toInt()
    val r = percent % x
    val roundFactor = if (r < x.toDouble() / 2) 0 else x
    val rounded = q * x + roundFactor
    return rounded.toDouble() / 100
}




/**
 * An function similar to Excel's `mRound()` that rounds a value to the nearest increment.
 * For example, if an axis that provides a value between 0 and 15, but you want distinct steps
 * of `3` values, that is `0, 3, 6, 9, 12, 15`, then this method would be called
 * with an increment of `3`.
 */
fun Int.roundToNearestIncrement(increment: Int): Int = (this.toDouble().roundToNearestIncrement(increment.toDouble())).toInt()

/**
 * An function similar to Excel's `mRound()` that rounds a value to the nearest increment.
 * For example, if an axis that provides a value between 0 and 15, but you want distinct steps
 * of `3` values, that is `0, 3, 6, 9, 12, 15`, then this method would be called
 * with an increment of `3`.
 */
fun Int.roundToNearestIncrement(increment: Int, min: Int, max: Int): Int
{
    require(min < max) { "For 'Int.toNearestIncrement' the min value must be less than the max value. Got: min: $min  max: $max " }
    if (this <= min) return min
    if (this >= max) return max
    val result = this.roundToNearestIncrement(increment)
    return result.constrain(min, max)
}

/**
 * An function similar to Excel's `mRound()` that rounds a value to the nearest increment.
 * For example, if an axis that provides a value between 0 and 1, but you want distinct steps
 * of `.2` values, that is `0, .2, .4, .6, .8, 1.0`, then this method would be called
 * with an increment of `0.2`.
 */
fun Double.roundToNearestIncrement(increment: Double): Double = (this / increment).roundToInt() * increment

/**
 * A function that adjusts a value to the nearest increment. For example, if
 * an axis that provides a value between 0 and 1, but you want distinct steps
 * of `.2` values, that is 0, .2, .4, .6, .8, and 1.0, then this method would be called
 * with an increment of `0.2`. This function also constrains the result between a min and
 * max value.
 */
fun Double.roundToNearestIncrement(increment: Double, min: Double, max: Double): Double
{
    require(min < max) { "For 'Double.toNearestIncrement' the min value must be less than the max value. Got: min: $min  max: $max " }
    if (this <= min) return min
    if (this >= max) return max
    val result = this.roundToNearestIncrement(increment)
    return result.constrain(min, max)
}

/**
 * A function that adjusts a value to the nearest increment. For example, if
 * an axis that provides a value between 0 and 1, but you want distinct steps
 * of `.2` values, that is 0, .2, .4, .6, .8, and 1.0, then this method would be called
 * with an increment of `0.2`. This function also constrains the result between a min and
 * max value.
 */
fun Double.roundToNearestIncrement(increment: Double, min: Int, max: Int): Double
{
    require(min < max) { "For 'Double.toNearestIncrement' the min value must be less than the max value. Got: min: $min  max: $max " }
    if (this <= min) return min.toDouble()
    if (this >= max) return max.toDouble()
    val result = this.roundToNearestIncrement(increment)
    return result.constrain(min, max)
}


/**
 * Primarily for Java use, or when debug logging is desired.
 * For kotlin, use a range check:
 * ```
 * val result = number in 10..20
 * ```
 */
fun Double.isBetween(lowerLimitInclusive: Double, upperLimitInclusive: Double): Boolean
{
    val result = this in lowerLimitInclusive..upperLimitInclusive
    logger.debug { "Checking if $this isBetween $lowerLimitInclusive and $upperLimitInclusive; result = $result" }
    return result
}

/**
 * Primarily for Java use, or when debug logging is desired.
 * For kotlin, use a range check:
 * ```
 * val result = number in 10..20
 * ```
 */
fun Int.isBetween(lowerLimitInclusive: Int, upperLimitInclusive: Int): Boolean
{
    val result = this in lowerLimitInclusive..upperLimitInclusive
    logger.debug { "Checking if $this isBetween $lowerLimitInclusive and $upperLimitInclusive; result = $result" }
    return result
}


fun Double.isNear(targetValue: Double, deltaInclusive: Double): Boolean = isBetween(targetValue - deltaInclusive, targetValue + deltaInclusive)


fun Int.isNear(targetValue: Int, deltaInclusive: Int): Boolean = isBetween(targetValue - deltaInclusive, targetValue + deltaInclusive)



