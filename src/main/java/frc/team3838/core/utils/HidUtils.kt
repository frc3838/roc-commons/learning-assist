package frc.team3838.core.utils

//import edu.wpi.first.hal.HAL
import edu.wpi.first.wpilibj.DriverStation
import edu.wpi.first.wpilibj.GenericHID
import frc.team3838.core.meta.API

object HidUtils
{
    @API
    @JvmStatic
    fun isHidConnected(hidPort: Int): Boolean = DriverStation.getInstance().getJoystickAxisType(hidPort, 0) != -1

    @API
    @JvmStatic
    fun isGamepad(hid: GenericHID?): Boolean = hid != null && isGamepad(hid.port)

    @API
    @JvmStatic
    /**
     * Determines if the hid is a GamePad. Note that the driver station reports both the
     * Logitech F310 and the XBox Controller as 'XBox'.
     */
    fun isGamepad(hidPort: Int): Boolean = isHidConnected(hidPort) && DriverStation.getInstance().getJoystickIsXbox(hidPort)

//    @API
//    @JvmStatic
//    fun getHidName(hid: GenericHID): String = getHidName(hid.port)

//    @API
//    @JvmStatic
//    fun getHidName(hidPort: Int): String = HAL.getJoystickName(hidPort.toByte())

    @API
    @JvmStatic
    fun getGamePadDefaultXAxis(thumbStickSide: GenericHID.Hand): Int
    {
        return when (thumbStickSide)
        {
            GenericHID.Hand.kLeft  -> 1
            GenericHID.Hand.kRight -> 4
        }
    }
    @API
    @JvmStatic
    fun getGamePadDefaultYAxis(thumbStickSide: GenericHID.Hand): Int
    {
        return when (thumbStickSide)
        {
            GenericHID.Hand.kLeft  -> 2
            GenericHID.Hand.kRight -> 5
        }
    }
}