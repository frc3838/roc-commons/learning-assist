@file:Suppress("unused")

package frc.team3838.core.utils

import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableSet
import com.google.common.collect.ImmutableSortedSet
import frc.team3838.core.meta.API
import java.util.stream.Collectors
import java.util.stream.Stream


/**
 * Converts an `Iterable` to a Guava [ImmutableSet].
 */
@API
fun <T> Iterable<T>.toImmutableSet(): ImmutableSet<T>
{
    return if (this is Collection)
    {
        ImmutableSet.copyOf(this)
    }
    else
    {
        val builder = ImmutableSet.builder<T>()
        this.forEach { builder.add(it) }
        builder.build()
    }
}

/**
 * Returns a Guava [ImmutableSet] of all elements.
 *
 * The operation is _terminal_.
 */
@API
fun <T> Sequence<T>.toImmutableSet(): ImmutableSet<T>
{
    val builder = ImmutableSet.builder<T>()
    for (item in this)
    {
       builder.add(item) 
    }
    
    return builder.build()
}

/**
 * Returns a Guava [ImmutableSet] of elements produced by this stream..
 *
 * The operation is _terminal_.
 */
@API
fun <T> Stream<T>.toImmutableSet(): ImmutableSet<T> = ImmutableSet.copyOf(this.collect(Collectors.toSet()))


/**
 * Converts an `Iterable` to a Guava [ImmutableSortedSet].
 */
@API
fun <T : Comparable<T>?> Iterable<T>.toImmutableSortedSet(): ImmutableSortedSet<T>
{
    return if (this is Collection)
    {
        ImmutableSortedSet.copyOf(this)
    }
    else
    {
        val builder = ImmutableSortedSet.naturalOrder<T>()
        this.forEach { builder.add(it) }
        builder.build()
    }
}

/**
 * Converts an `Iterable` to a Guava [ImmutableSortedSet].
 */
@API
fun <T> Iterable<T>.toImmutableSortedSet(comparator: Comparator<T>): ImmutableSortedSet<T>
{
    return if (this is Collection)
    {
        ImmutableSortedSet.copyOf(comparator,this)
    }
    else
    {
        val builder = ImmutableSortedSet.orderedBy<T>(comparator)
        this.forEach { builder.add(it) }
        builder.build()
    }
}


/**
 * Returns a Guava [ImmutableSortedSet] of all elements.
 *
 * The operation is _terminal_.
 */
fun <T : Comparable<T>?> Sequence<T>.toImmutableSortedSet(): ImmutableSortedSet<T>
{
    val builder = ImmutableSortedSet.naturalOrder<T>()
    for (item in this)
    {
       builder.add(item) 
    }
    
    return builder.build()
}

/**
 * Returns a Guava [ImmutableSortedSet] of all elements.
 *
 * The operation is _terminal_.
 */
fun <T> Sequence<T>.toImmutableSortedSet(comparator: Comparator<T>): ImmutableSortedSet<T>
{
    val builder = ImmutableSortedSet.orderedBy<T>(comparator)
    for (item in this)
    {
       builder.add(item)
    }

    return builder.build()
}


/**
 * Returns a Guava [ImmutableSortedSet] of elements produced by this stream..
 *
 * The operation is _terminal_.
 */
fun <T : Comparable<T>?> Stream<T>.toImmutableSortedSet(): ImmutableSortedSet<T> = ImmutableSortedSet.copyOf(this.collect(Collectors.toSet()))

/**
 * Returns a Guava [ImmutableSortedSet] of elements produced by this stream..
 *
 * The operation is _terminal_.
 */
fun <T> Stream<T>.toImmutableSortedSet(comparator: Comparator<T>): ImmutableSortedSet<T> = ImmutableSortedSet.copyOf(comparator, this.collect(Collectors.toSet()))


/**
 * Converts an `Iterable` to a Guava [ImmutableList].
 */
fun <T> Iterable<T>.toImmutableList(): ImmutableList<T>
{
    return if (this is Collection)
    {
        ImmutableList.copyOf(this)
    }
    else
    {
        val builder = ImmutableList.builder<T>()
        this.forEach { builder.add(it) }
        builder.build()
    }
}

/**
 * Returns a Guava [ImmutableList] of all elements.
 *
 * The operation is _terminal_.
 */
fun <T> Sequence<T>.toImmutableList(): ImmutableList<T>
{
    val builder = ImmutableList.builder<T>()
    for (item in this)
    {
       builder.add(item) 
    }
    
    return builder.build()
}

/**
 * Returns a Guava [List] of elements produced by this stream..
 *
 * The operation is _terminal_.
 */
fun <T> Stream<T>.toImmutableList(): ImmutableList<T> = ImmutableList.copyOf(this.collect(Collectors.toList()))



/**
 * Converts an `Iterable` to a Guava [ImmutableList], with elements sorted in the list according to the
 * supplied [Comparator].
 */
fun <T> Iterable<T>.toImmutableListSorted(comparator: Comparator<T>): ImmutableList<T>
{
    return if (this is Collection)
    {
        ImmutableList.sortedCopyOf(comparator,this)
    }
    else
    {
        ImmutableList.sortedCopyOf(comparator, this.toMutableList())
    }
}

/**
 * Converts an `Iterable` to a Guava [ImmutableList], with elements sorted in the list according to their
 * natural sort order.
 */
fun <T : Comparable<T>?> Iterable<T>.toImmutableListSorted(): ImmutableList<T>
{
    return if (this is Collection)
    {
        ImmutableList.sortedCopyOf(this)
    }
    else
    {
        ImmutableList.sortedCopyOf(this.toMutableList())
    }
}

/**
 * Converts an `Iterable` to a Guava [ImmutableList], with elements sorted in the list according to the
 * natural sort order of the value returned by specified [selector] function.
 */
inline fun <T, R : Comparable<R>> Iterable<T>.toImmutableListSortedBy(crossinline selector: (T) -> R?): ImmutableList<T>
{
    return if (this is Collection)
    {
        ImmutableList.copyOf(this.sortedBy(selector))
    }
    else
    {
        val list = this.toMutableList()
        list.sortBy(selector)
        ImmutableList.copyOf(list)
    }
}


/**
 * Returns a Guava [ImmutableList] of all elements, with elements sorted in the list according to their
 * natural sort order.
 *
 * The operation is _terminal_.
 */
fun <T : Comparable<T>?> Sequence<T>.toImmutableListSorted(): ImmutableList<T> = ImmutableList.sortedCopyOf(this.toMutableList())

/**
 * Returns a Guava [ImmutableList] of all elements, with elements sorted in the list according to the
 * supplied [Comparator].
 *
 * The operation is _terminal_.
 */
fun <T> Sequence<T>.toImmutableListSorted(comparator: Comparator<T>): ImmutableList<T> = ImmutableList.sortedCopyOf(comparator, this.toMutableList())

/**
 * Converts a `Sequence` to a Guava [ImmutableList], with elements sorted in the list according to the
 * natural sort order of the value returned by specified [selector] function.
 */
inline fun <T, R : Comparable<R>> Sequence<T>.toImmutableListSortedBy(crossinline selector: (T) -> R?): ImmutableList<T>
{
        val list = this.toMutableList()
        list.sortBy(selector)
        return ImmutableList.copyOf(list)
}


/**
 * Returns a Guava [ImmutableList] of elements produced by this stream, with elements sorted in the list according to their
 * natural sort order.
 *
 * The operation is _terminal_.
 */
fun <T : Comparable<T>?> Stream<T>.toImmutableListSorted(): ImmutableList<T> = ImmutableList.sortedCopyOf(this.collect(Collectors.toList()))

/**
 * Returns a Guava [ImmutableList] of elements produced by this stream, with elements sorted in the list according to the
 * supplied [Comparator].
 *
 * The operation is _terminal_.
 */
fun <T> Stream<T>.toImmutableListSorted(comparator: Comparator<T>): ImmutableList<T> = ImmutableList.sortedCopyOf(comparator, this.collect(Collectors.toList()))

/**
 * Returns a Guava [ImmutableList], with elements sorted in the list according to the
 * natural sort order of the value returned by specified [selector] function.
 */
inline fun <T, R : Comparable<R>> Stream<T>.toImmutableListSortedBy(crossinline selector: (T) -> R?): ImmutableList<T>
{
    val list = this.collect(Collectors.toList())
    list.sortBy(selector)
    return ImmutableList.copyOf(list)
}


/**
 * Creates a Guava [ImmutableList] of elements.
 */
fun <T> immutableListOf(first: T, vararg others: T): ImmutableList<T>
{
    val builder = ImmutableList.builder<T>()
    builder.add(first)
    others.forEach { builder.add(it) }
    return builder.build()
}

/**
 * Creates a Guava [ImmutableList] with elements sorted in the list according to the
 * supplied [Comparator].
 */
fun <T> immutableListOfSorted(comparator: Comparator<T>, first: T, vararg others: T): ImmutableList<T>
{
    val list = mutableListOf(first)
    list.addAll(others)
    return ImmutableList.sortedCopyOf(comparator, list)
}

/**
 * Creates a Guava [ImmutableList] with elements sorted in the list according to their
 * natural sort order.
 */
fun <T : Comparable<T>?> immutableListOfSorted(first: T, vararg others: T): ImmutableList<T>
{
    val list = mutableListOf(first)
    list.addAll(others)
    return ImmutableList.sortedCopyOf(list)
}

/**
 * Creates a Guava [ImmutableList] with elements sorted in the list according to the
 * natural sort order of the value returned by specified [selector] function.
 *
 * The sort is _stable_, meaning that equal elements preserve their order relative to each other after sorting.
 */
inline fun <T, R : Comparable<R>> immutableListOfSortedBy(first: T, vararg others: T, crossinline selector: (T) -> R?): ImmutableList<T>
{
    val list = mutableListOf(first)
    list.addAll(others)
    list.sortBy(selector)
    return ImmutableList.copyOf(list)
}



fun <T> immutableSetOf(first: T, vararg others: T): ImmutableSet<T>
{
    val builder = ImmutableSet.builder<T>()
    builder.add(first)
    others.forEach { builder.add(it) }
    return builder.build()
}

fun <T : Comparable<T>?> immutableSortedSetOf(first: T, vararg others: T): ImmutableSortedSet<T>
{
    val builder = ImmutableSortedSet.naturalOrder<T>()
    builder.add(first)
    others.forEach { builder.add(it) }
    return builder.build()
}

fun <T> immutableSortedSetOf(comparator: Comparator<T>, first: T, vararg others: T): ImmutableSortedSet<T>
{
    val builder = ImmutableSortedSet.orderedBy<T>(comparator)
    builder.add(first)
    others.forEach { builder.add(it) }
    return builder.build()
}

