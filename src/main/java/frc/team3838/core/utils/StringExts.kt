package frc.team3838.core.utils

/**
 * Pads all lines, in a multiline CharSequence, with the provided character.
 *
 * @see padEnd
 */
fun CharSequence.padLineEnds(desiredLineLength: Int, padChar: Char = ' '): CharSequence
{
    val sb = StringBuilder()
    this.split("""[\r\n]{1,2}""".toRegex()).forEach {
        sb.appendLine(it.trim().padEnd(desiredLineLength, padChar))
    }
    return sb
}