package frc.team3838.core.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.status.ErrorStatus;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.status.WarnStatus;
import edu.wpi.first.wpilibj.DriverStation;
import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import static ch.qos.logback.classic.Level.*;



/**
 * <p>
 * Because the roboRIO uses a compact2 JRE, we cannot use a standard logback XML file as the
 * XML based classes used to parse it are not available in a compact2 profile. We therefore
 * configure the logging programmatically. There is an outstanding to do to see if you can use
 * the groovy based logging configuration (i.e. logback.groovy) that Logback provides. But it
 * has not yet been explored (as this methodology works ok for our simple needs). For more
 * information on Java profiles, see:
 * </p>
 * <ol>
 * <li><a href='https://blogs.oracle.com/jtc/entry/a_first_look_at_compact'>An Introduction to Java 8 Compact Profiles</a></li>
 * <li><a href='http://www.oracle.com/technetwork/java/embedded/resources/tech/compact-profiles-overview-2157132.html'>Java SE Embedded 8 Compact Profiles Overview</a></li>
 * </ol>
 * <p>
 * <strong>USAGE:</strong><br>
 * Add the following statement as the first statement in {@code robotInit()}:<br>
 * </p>
 * <pre>
 * LogbackProgrammaticConfiguration.init();
 * </pre>
 * <p>
 * The {@link #init()} method is guaranteed to not throw an exception.
 * </p>
 */
@SuppressWarnings({"UseOfSystemOutOrSystemErr", "UnusedDeclaration", "CallToPrintStackTrace", "ConstantConditions"})
public class LogbackProgrammaticConfiguration
{
    private static final Logger logger = LoggerFactory.getLogger(LogbackProgrammaticConfiguration.class);


    /**
     * Boolean flag meant for use to guard Smart Dashboard Debug data or other Debugging output.
     * <strong>Setting this value does NOT activate DEBUG logging.</strong> Use {@link #rootLevel}
     * to change logging output level.
     */
    public static final boolean IN_DEBUGGING_MODE = false;


    public static final boolean IN_COMPETITION = DriverStation.getInstance().isFMSAttached();

    /** The ROOT logger level. */
    public static final Level rootLevel = IN_COMPETITION ? INFO : DEBUG;

    /** If logging framework should output initialization debug information. */
    public static final boolean logInitializationDebug = false;

    /** A temporary Map that is used during the initialization of the loggers. */
    private final static Map<String, Level> loggerLevelsInitializationMap = new HashMap<>();



    // === SET INDIVIDUAL LOGGING LEVELS HERE ===
    static
    {
        try
        {
            // The first level is the level used during dev work in the build season.
            // The second level is the level during the completion and typically should be INFO, WARN, or ERROR

            set(LogTester.class, OFF, OFF);
            //Default all code to a default level. For competitions, this should be WARN
            setPackage("frc.team3838", INFO, WARN);


            // === PRIMARY CLASSES ===
            // We want the primary Robot class to be INFO for Competitions

            //set(Robot3838.class, INFO, INFO);
    
            // Subsystems
            final Level subsystemsLevel = INFO;
            


            // === LIBRARY CLASSES ===
            setPackage("org.reflections", WARN, WARN);

            // -----------------------------------------------------------------------
            //
            //
            //
            // ***** DO NOT PUT ANY LOG SETTINGS/CONFIGURATIONS BELOW THIS POINT *****
        }
        catch (Throwable e)
        {
            try
            {
                System.err.println("Could not set logging levels due to exception: " + e.toString());
                e.printStackTrace();
            }
            catch (Throwable ignore)
            {
                // The above System.err calls should really never throw an exception... but we want to be sure
            }
        }
    }
    //@formatter:off




































    // ***** INTERNAL INITIALIZATION AND OPERATION CODE *****
    //@formatter:on

    /**
     * Flag that is automatically set based on the {@link #rootLevel} setting such that additional detail is
     * added to the logging output, namely the {@code caller} detail.
     */
    private static final boolean WITH_DETAIL = false; // (rootLevel.equals(DEBUG) || rootLevel.equals(TRACE) || rootLevel.equals(ALL));


    //private static final String consolePatternLayoutBASE = "%date{HH:mm:ss.SSS zzz} %highlight(%-5level) [%-5thread] %-40logger{40} - %message";
    private static final String consolePatternWithColor = "%date{HH:mm:ss.SSS zzz} %highlight(%-5level) %cyan(%-40logger{40}) %boldBlue(::) %message";
    private static final String consolePatternNoColor = "%date{HH:mm:ss.SSS zzz} %-5level %-40logger{40} :: %message";
    private static final String consolePatternLayoutBASE = consolePatternNoColor;
    private static final String consolePatternLayoutSUFFIX = WITH_DETAIL
                                                             ? "      \t\t==> %caller{1}"
                                                             : "%n";

    private static final String consolePatternLayout = consolePatternLayoutBASE + consolePatternLayoutSUFFIX;


    //Used when reporting status messages to indicate the origin of the message
    @SuppressWarnings("InstantiationOfUtilityClass")
    private static final LogbackProgrammaticConfiguration origin = new LogbackProgrammaticConfiguration();


    private LogbackProgrammaticConfiguration() { }


    /**
     * Sets the log level for a class. For example:
     * <pre>
     * set(CatapultSubSystem.class, WARN, INFO);
     * set(FireCatapultCommand.class, WARN, DEBUG);
     * </pre>
     *
     * @param clazz                the class to set the log level for
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     */
    private static void set(@Nonnull Class<?> clazz, @Nonnull Level developmentWorkLevel, @Nonnull Level competitionLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        _internalInitAddLogger(clazz.getName(), level);
    }


    /**
     * Sets the log level for a logger by name. This generally should be the fully qualified class name.
     * As such, it is preferable to use the {@link #set(Class, Level, Level)} method.
     *
     * @param loggerName           the name of the logger
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     */
    private static void set(@Nonnull String loggerName, @Nonnull Level developmentWorkLevel, @Nonnull Level competitionLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        _internalInitAddLogger(loggerName, level);
    }


    /**
     * Sets the log level for a package, and thus all classes in that package and ay of its sub-packages.
     *
     * @param pkg                  the package to set the log level for.
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     */
    private static void setPackage(@Nonnull Package pkg, @Nonnull Level developmentWorkLevel, @Nonnull Level competitionLevel)
    {
        final Level level = IN_COMPETITION ? competitionLevel : developmentWorkLevel;
        _internalInitAddLogger(pkg.getName(), level);
    }


    /**
     * Sets the log level for a package, and thus all classes in that package and ay of its sub-packages.
     * This package set will be the package of the provided class. This is done for convenience and is the
     * equivalent of {@code  setPackage(clazz.getPackage(), competitionLevel, developmentWorkLevel)}
     *
     * @param clazz                a class in the package to set the log level for
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     */
    private static void setPackage(@Nonnull Class<?> clazz, @Nonnull Level developmentWorkLevel, @Nonnull Level competitionLevel)
    {
        setPackage(clazz.getPackage(), developmentWorkLevel, competitionLevel);
    }


    /**
     * Sets the log level for a package by name, and thus all classes in that package and ay of its sub-packages.
     *
     * @param packageName          the name of the package to set the log level for
     * @param developmentWorkLevel the log level to use outside competition when doing development work.
     *                             In general this should be INFO unless actively debugging a system, in
     *                             which case it can be set to DEBUG or TRACE. It may also be set to WARN,
     *                             ERROR, or OFF if it is desired reduce logging 'noise' from systems that
     * @param competitionLevel     the log level to use during competition. In general, this should be WARN,
     *                             or possibly INFO as we want to reduce log verbosity during competition
     */
    @SuppressWarnings("SameParameterValue")
    private static void setPackage(@Nonnull String packageName, @Nonnull Level developmentWorkLevel, @Nonnull Level competitionLevel)
    {
        set(packageName, developmentWorkLevel, competitionLevel);
    }


    private static void _internalInitAddLogger(@Nonnull String loggerName, @Nonnull Level level)
    {
        @Nullable
        final Level previous = loggerLevelsInitializationMap.put(loggerName, level);
        if (previous != null && level.equals(previous))
        {
            reportWarning(
                "logger '" + loggerName + "' configured multiple times as '" + previous + "' and '" + level + "'. It will be set to  '" + level + "'.");
        }
    }


    /**
     * Initializes the logging framework. This should be called as the first statement (ir as early as possible)
     * in the {@code robotInit()} method. <strong>This method is guaranteed to never throw an exception.</strong>
     */
    public static void init()
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                reportInfo("Programmatically configuring logging...");
                reportInfo("    Logging settings:");
                reportInfo("        In Competition:    " + IN_COMPETITION);
                reportInfo("        In Debugging Mode: " + IN_DEBUGGING_MODE);

                reportDebug("Resetting Context");
                context.reset();

                reportDebug("Creating Console Appender");
                ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<>();
                consoleAppender.setContext(context);
                consoleAppender.setName("CONSOLE");

                reportDebug("Creating Pattern Layout");
                PatternLayoutEncoder patternLayout = new PatternLayoutEncoder();
                patternLayout.setContext(context);
                patternLayout.setPattern(consolePatternLayout);
                reportDebug("Starting Pattern Layout");
                patternLayout.start();

                consoleAppender.setEncoder(patternLayout);
                reportDebug("Starting Console Appender");
                consoleAppender.start();

                reportDebug("Getting Root Logger");
                final ch.qos.logback.classic.Logger rootLogger = context.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
                rootLogger.addAppender(consoleAppender);
                reportDebug("Setting Root Logger Level to " + rootLevel);
                rootLogger.setLevel(rootLevel);

                reportDebug("Configuring various logging levels");
                if (loggerLevelsInitializationMap != null)
                {
                    for (Map.Entry<String, Level> entry : loggerLevelsInitializationMap.entrySet())
                    {
                        final String loggerName = entry.getKey();
                        final Level level = entry.getValue();
                        context.getLogger(loggerName).setLevel(level);
                    }
                }

                reportInfo("Configuring System out and err over SLF4J");
                SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.INFO, LogLevel.ERROR);
                reportInfo("Programmatic logging configuration has completed");
                reportInfo("    Logging settings:");
                reportInfo("        In Competition:    " + IN_COMPETITION);
                reportInfo("        In Debugging Mode: " + IN_DEBUGGING_MODE);

                //Give time for full initialization
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}

                LogTester.log();
            }
            else
            {
                reportError(">>>>ERROR<<<< Could not Programmatically configuring logging since a reference to the LoggerContext could not be obtained.");
            }

        }
        catch (Throwable e)
        {
            try
            {
                // While reportError() should never throw an exception... we want to be _absolutely_ sure this init() method never does.
                reportError(e);
            }
            catch (Throwable ignore) { }
        }
    }


    /**
     * Reports/logs an error message, via the Logback Context StatusManager to the console at
     * an 'error' status level.
     *
     * @param msg the message to log
     */
    protected static void reportError(@Nonnull String msg)
    {
        try
        {
            reportError(msg, null);
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * an 'error' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportError(@Nonnull Throwable t)
    {
        try
        {
            reportError("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs an error message and an exception, via the Logback Context StatusManager
     * to the console at an 'error' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportError(@Nonnull String msg, @Nullable Throwable t)
    {
        try
        {
            final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION ERROR: ").append(msg);
            if (t != null)
            {
                fullMessage.append(" :: Exception: ").append(t.toString());
            }
            System.err.println(fullMessage.toString());
            reportStatus(new ErrorStatus(fullMessage.toString(), origin, t));
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs a warning message, via the Logback Context StatusManager to the console at
     * a 'warn' status level.
     *
     * @param msg the message to log
     */
    protected static void reportWarning(@Nonnull String msg)
    {
        try
        {
            reportWarning(msg, null);
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs an exception, via the Logback Context StatusManager to the console at
     * a 'warn' status level. A default message, indicating an exception occurred, is used when
     * reporting the exception.
     *
     * @param t the throwable/exception to report
     */
    protected static void reportWarning(@Nonnull Throwable t)
    {
        try
        {
            reportWarning("An exception occurred during programmatic logging configuration via the " + LogbackProgrammaticConfiguration.class.getName(), t);
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs an warning message and an exception, via the Logback Context StatusManager
     * to the console at the 'warn' status level.
     *
     * @param msg the message to log
     * @param t   the throwable/exception to report; if {@code null} only the message is logged
     */
    protected static void reportWarning(@Nonnull String msg, @Nullable Throwable t)
    {
        try
        {
            final StringBuilder fullMessage = new StringBuilder("LOGGING CONFIGURATION WARNING: ").append(msg);
            if (t != null)
            {
                fullMessage.append(" :: Exception: ").append(t.toString());
            }
            System.out.println(fullMessage);

            reportStatus(new WarnStatus(fullMessage.toString(), origin, t));
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs an informational message, via the Logback Context StatusManager to the console at
     * an 'info' status level.
     *
     * @param msg the message to log
     */
    protected static void reportInfo(@Nonnull String msg)
    {
        try
        {
            final String fullMessage = "LOGGING CONFIGURATION INFORMATION: " + msg;
            System.out.println(fullMessage);
            reportStatus(new InfoStatus(fullMessage, origin));
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * Reports/logs a debug message, via the Logback Context StatusManager to the console at
     * an 'debug' status level.
     *
     * @param msg the message to log
     */
    protected static void reportDebug(@Nonnull String msg)
    {
        try
        {
            if (logInitializationDebug)
            {
                final String fullMessage = "LOGGING CONFIGURATION DEBUG INFO:  " + msg;
                System.out.println(fullMessage);
                reportStatus(new InfoStatus(fullMessage, origin));
            }
        }
        catch (Throwable ignore)
        {
            // The above should really never throw an exception... but we want to be sure
        }
    }


    /**
     * An internal method used for reporting statuses.
     *
     * @param status the status to report
     */
    protected static void reportStatus(Status status)
    {
        try
        {
            final LoggerContext context = getLoggerContext();
            if (context != null)
            {
                StatusManager sm = context.getStatusManager();
                if (sm != null)
                {
                    sm.add(status);
                }
            }
        }
        catch (Throwable e)
        {
            try
            {
                System.err.println("An exception occurred when attempting to report/log a Logback status. Cause details: " + e);
                e.printStackTrace();
            }
            catch (Throwable ignore)
            {
                // The above should really never throw an exception... but we want to be sure
            }
        }
    }


    @Nullable
    protected static LoggerContext getLoggerContext()
    {
        try
        {
            // assume SLF4J is bound to logback in the current environment
            return (LoggerContext) LoggerFactory.getILoggerFactory();
        }
        catch (Throwable e)
        {
            try
            {
                System.err.println("\">>>>ERROR<<<< Could not get LoggerContext reference. Cause Details: " + e.toString());
                e.printStackTrace();
                System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                return null;
            }
            catch (Throwable ignore)
            {
                // The above should really never throw an exception... but we want to be sure
                return null;
            }
        }
    }
}
