package frc.team3838.core.logging

import frc.team3838.core.meta.API


interface BorderStyle
{
    val borderTopLeft: Char
    val borderHorizontal: Char
    val borderTopColumn: Char
    val borderTopRight: Char
    val borderVertical: Char
    val borderLeftHeaderRow: Char
    val borderRightHeaderRow: Char
    val borderLeftDataRow: Char
    val borderRightDataRow: Char
    val borderBottomLeft: Char
    val borderBottomColumn: Char
    val borderBottomRight: Char
    val borderRowColumnCross: Char
    val innerHorizontal: Char
    val innerVertical: Char
    val innerRowColumnCross: Char

}

abstract class AbstractBorderStyle(
        override val borderTopLeft: Char,
        override val borderHorizontal: Char,
        override val borderTopColumn: Char,
        override val borderTopRight: Char,
        override val borderVertical: Char,
        override val borderLeftHeaderRow: Char,
        override val borderRightHeaderRow: Char,
        override val borderLeftDataRow: Char,
        override val borderRightDataRow: Char,
        override val borderBottomLeft: Char,
        override val borderBottomColumn: Char,
        override val borderBottomRight: Char,
        override val borderRowColumnCross: Char,
        override val innerHorizontal: Char,
        override val innerVertical: Char,
        override val innerRowColumnCross: Char
                                  ) : BorderStyle

object BorderStyleBoxDrawingBasic : AbstractBorderStyle('╔', '═', '╤', '╗',
                                                        '║', '╠', '╣', '╟', '╢',
                                                        '╚', '╧', '╝',
                                                        '╪',
                                                        '─', '│', '┼'
                                                       )
@API
object BorderStyleAscii : AbstractBorderStyle('+', '=', '+', '+',
                                              '|', '+', '+', '+', '+',
                                              '+', '+', '+',
                                              '+',
                                              '-', '|', '+'
                                             )
@API
object BorderStyleAsciiLight : AbstractBorderStyle('+', '-', '+', '+',
                                                   '|', '+', '+', '+', '+',
                                                   '+', '+', '+',
                                                   '+',
                                                   '-', '|', '+'
                                                  )
@API
object BorderStyleBoxDrawing : AbstractBorderStyle('┏', '━', '┯', '┓',
                                                   '┃', '┣', '┫', '┠', '┨',
                                                   '┗', '┷', '┛',
                                                   '┿',
                                                   '─', '│', '┼'
                                                  )
@API
class TextTable private constructor(private val headers: List<*>, private val data: List<List<*>>, private val borderStyle: BorderStyle = BorderStyleBoxDrawingBasic)
{
    private val columns: Int = headers.size
    private val columnWidths: IntArray
    private val emptyWidth: Int


    init
    {

        columnWidths = IntArray(columns)
        for (row in -1 until data.size)
        {
            val rowData = if (row == -1) headers else data[row] // Hack to parse headers too.
            if (rowData.size != columns)
            {
                throw IllegalArgumentException(
                        String.format("Row %s's %s columns != %s columns", row + 1, rowData.size, columns))
            }
            for (column in 0 until columns)
            {
                for (rowDataLine in rowData[column].toString().split("\\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
                {
                    columnWidths[column] = columnWidths[column].coerceAtLeast(rowDataLine.length)
                }
            }
        }

        var emptyWidth = 3 * (columns - 1) // Account for column dividers and their spacing.
        for (columnWidth in columnWidths)
        {
            emptyWidth += columnWidth
        }
        this.emptyWidth = emptyWidth

        if (emptyWidth < EMPTY.length)
        { // Make sure we're wide enough for the empty text.
            columnWidths[columns - 1] += EMPTY.length - emptyWidth
        }
    }


    override fun toString(): String
    {
        val builder = StringBuilder()
        toLines().forEach { builder.appendLine(it) }
        return builder.toString()
    }

    @API
    fun toLines(): List<String>
    {
        val lines = mutableListOf<String>()
        printDivider(lines, "${borderStyle.borderTopLeft}${borderStyle.borderHorizontal}${borderStyle.borderTopColumn}${borderStyle.borderHorizontal}${borderStyle.borderTopRight}")
        printData(lines, headers)
        if (data.isEmpty())
        {
            printDivider(lines, "${borderStyle.borderLeftHeaderRow}${borderStyle.borderHorizontal}${borderStyle.borderBottomColumn}${borderStyle.borderHorizontal}${borderStyle.borderRightHeaderRow}")
            val builder = StringBuilder()
            builder.append(borderStyle.borderVertical).append(pad(emptyWidth, EMPTY)).append("${borderStyle.borderVertical}")
            lines.add(builder.toString())
            printDivider(lines, "${borderStyle.borderBottomLeft}${borderStyle.borderHorizontal}${borderStyle.borderHorizontal}${borderStyle.borderHorizontal}${borderStyle.borderBottomRight}")
        }
        else
        {
            for (row in data.indices)
            {
                printDivider(lines, if (row == 0) "${borderStyle.borderLeftHeaderRow}${borderStyle.borderHorizontal}${borderStyle.borderRowColumnCross}${borderStyle.borderHorizontal}${borderStyle.borderRightHeaderRow}" else "${borderStyle.borderLeftDataRow}${borderStyle.innerHorizontal}${borderStyle.innerRowColumnCross}${borderStyle.innerHorizontal}${borderStyle.borderRightDataRow}")
                printData(lines, data[row])
            }
            printDivider(lines, "${borderStyle.borderBottomLeft}${borderStyle.borderHorizontal}${borderStyle.borderBottomColumn}${borderStyle.borderHorizontal}${borderStyle.borderBottomRight}")
        }
        return lines
    }


    private fun printDivider(lines: MutableList<String>, format: String)
    {
        val out = StringBuilder()
        for (column in 0 until columns)
        {
            out.append(if (column == 0) format[0] else format[2])
            out.append(pad(columnWidths[column], "").replace(' ', format[1]))
        }
        out.append(format[4])
        lines.add(out.toString())
    }


    private fun printData(lines: MutableList<String>, data: List<*>)
    {
        val out = StringBuilder()
        var currentLine = 0
        var linesCount = 1
        while (currentLine < linesCount)
        {
            for (column in 0 until columns)
            {
                out.append(if (column == 0) borderStyle.borderVertical else borderStyle.innerVertical)
                val cellLines = data[column].toString().split("\\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                linesCount = linesCount.coerceAtLeast(cellLines.size)
                val cellLine = if (currentLine < cellLines.size) cellLines[currentLine] else ""
                out.append(pad(columnWidths[column], cellLine))
            }

            out.append("${borderStyle.borderVertical}")
            lines.add(out.toString())
            out.clear()
            currentLine++
        }
    }

    companion object
    {
        private const val EMPTY = "(empty)"

        @API
        @JvmOverloads
        fun <H, D> of(headers: Array<H>, data: Array<Array<D>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): TextTable = of(headers.map { it as Any }, data.map {
            it.map { datum -> datum as Any }
        }, borderStyle)

        @API
        @JvmOverloads
        fun of(headers: Collection<*>, data: Collection<Collection<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): TextTable = of(headers.toList(), data.map {
            it.toList()
        }, borderStyle)

        @API
        @JvmOverloads
        fun of(headers: List<*>, data: List<List<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): TextTable
        {
            if (headers.isEmpty()) throw IllegalArgumentException("Headers must not be empty.")
            return TextTable(headers, data, borderStyle)
        }

        @API
        @JvmOverloads
        fun of(map: Map<*, *>, headers: List<String> = listOf("Key", "Value"), borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): TextTable = of(headers, map.map { listOf(it.key, it.value) }, borderStyle)

        @API
        @JvmOverloads
        fun <H, D> asStringFor(headers: Array<H>, data: Array<Array<D>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): String = asStringFor(headers.map { it as Any }, data.map {
            it.map { datum -> datum as Any }
        }, borderStyle)

        @API
        @JvmOverloads
        fun asStringFor(headers: Collection<*>, data: Collection<Collection<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): String = asStringFor(headers.toList(), data.map {
            it
                .toList()
        }, borderStyle)

        @API
        @JvmOverloads
        fun asStringFor(headers: List<*>, data: List<List<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): String
        {
            if (headers.isEmpty()) throw IllegalArgumentException("Headers must not be empty.")
            return TextTable(headers, data, borderStyle).toString()
        }

        @API
        @JvmOverloads
        fun asStringFor(map: Map<*, *>, headers: List<String> = listOf("Key", "Value"), borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): String = asStringFor(headers, map.map { listOf(it.key, it.value) }, borderStyle)


        @API
        @JvmOverloads
        fun <H, D> asLinesFor(headers: Array<H>, data: Array<Array<D>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): List<String> = asLinesFor(headers.map { it as Any }, data.map {
            it.map { datum -> datum as Any }
        }, borderStyle)

        @API
        @JvmOverloads
        fun asLinesFor(headers: Collection<*>, data: Collection<Collection<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): List<String> = asLinesFor(headers.toList(), data.map {
            it
                .toList()
        }, borderStyle)

        @API
        @JvmOverloads
        fun asLinesFor(headers: List<*>, data: List<List<*>>, borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): List<String>
        {
            if (headers.isEmpty()) throw IllegalArgumentException("Headers must not be empty.")
            return TextTable(headers, data, borderStyle).toLines()
        }

        @API
        @JvmOverloads
        fun asLinesFor(map: Map<*, *>, headers: List<String> = listOf("Key", "Value"), borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): List<String> = asLinesFor(headers, map.map { listOf(it.key, it.value) }, borderStyle)


        private fun pad(width: Int, data: String): String
        {
            return String.format(" %1$-" + width + "s ", data)
        }
    }
}

@API
@JvmOverloads
fun Map<*, *>.textTable(headers: List<String> = listOf("Key", "Value"), borderStyle: BorderStyle = BorderStyleBoxDrawingBasic): String = TextTable.asStringFor(this, headers, borderStyle)

