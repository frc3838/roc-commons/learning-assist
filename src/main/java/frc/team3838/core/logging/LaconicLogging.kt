package frc.team3838.core.logging

import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import frc.team3838.core.utils.time.getFpgaTimeInMilliseconds
import mu.KLogger
import mu.KMarkerFactory
import mu.KotlinLogging
import org.slf4j.Logger
import org.slf4j.Marker
import org.slf4j.event.Level
import org.slf4j.event.Level.*
import org.slf4j.helpers.MessageFormatter
import java.util.concurrent.TimeUnit


/**
 * A logger that limits the frequency at which it *will allow* log messages
 * to be logged to prevent extremely verbose output. It is primarily for
 * use in tight loops or frequently called methods (such as those called
 * via polling or a scheduled thread). Once a log message has been logged
 * at particular verbosity level, another message will not be logged at that
 * level until the configured wait duration has passed. All logging calls at
 * that same interval are simply dropped during the wait duration. Once the
 * wait duration has expired, *the next logging call* at that level is actually
 * logged. This is different than the [PeriodicLogger] class which actually logs
 * a message at fixed intervals. The wait duration resets and begins again once a
 * message has been logged. While there is a slight performance cost of checking if
 * it is time to log a message, the cost is minimal and is significantly better
 * than the performance cost of saturating the logging framework.
 *
 * @see LaconicLeveledLogger
 */
@Suppress("DuplicatedCode", "PrivatePropertyName", "RedundantLambdaArrow")
@API
open class LaconicLogger(override val underlyingLogger: Logger, interval: TimeDuration) : KLogger
{
    private val ENTRY = KMarkerFactory.getMarker("ENTRY")
    private val EXIT = KMarkerFactory.getMarker("EXIT")

    private val THROWING = KMarkerFactory.getMarker("THROWING")
    private val CATCHING = KMarkerFactory.getMarker("CATCHING")
    private val EXIT_ONLY = "exit"
    private val EXIT_MESSAGE = "exit with ({})"
    private var intervalMs: Long = interval.toMillis()

    private val lastLogTimes = mutableMapOf<Level, Long>().withDefault { _ -> Long.MIN_VALUE }

    //private var lastLogTime: Long = 0

    private fun isTimeToLog(level: Level): Boolean
    {
        val currentTime = getFpgaTimeInMilliseconds()
        val isTime = currentTime >= lastLogTimes.getValue(level) + intervalMs
        if (isTime)
        {
            lastLogTimes[level] = currentTime
        }
        return isTime
    }


    @API
    constructor(logger: Logger, intervalDuration: Long, intervalTimeUnit: TimeUnit) : this(KotlinLogging.logger(logger.name), TimeDuration.of(intervalDuration, intervalTimeUnit))

    @API
    constructor(logger: Logger, interval: String) : this(KotlinLogging.logger(logger.name), TimeDuration.of(interval))

    /** Updates the interval to the supplied interval. */
    fun setInterval(interval: TimeDuration)
    {
        intervalMs = interval.toMillis()
    }

    /** Updates the interval to the supplied interval. */
    fun setInterval(intervalDuration: Long, intervalTimeUnit: TimeUnit)
    {
        intervalMs = intervalTimeUnit.toMillis(intervalDuration)
    }

    /** Updates the interval to the supplied interval, such that the supplied string parses to a TIme Duration. */
    fun setInterval(interval: String)
    {
        intervalMs = TimeDuration.of(interval).toMillis()
    }


    override fun getName(): String
    {
        return underlyingLogger.name
    }

    // region: TRACE
    /*
     _____ ___    _   ___ ___
    |_   _| _ \  /_\ / __| __|
      | | |   / / _ \ (__| _|
      |_| |_|_\/_/ \_\___|___|
    ==========================================================================================================================================================*/

    override fun isTraceEnabled(): Boolean
    {
        return underlyingLogger.isTraceEnabled
    }

    override fun trace(msg: String)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(msg)
    }

    override fun trace(format: String, arg: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(format, arg)
    }

    override fun trace(format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(format, arg1, arg2)
    }

    override fun trace(format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(format, *arguments)
    }

    override fun trace(msg: String, t: Throwable)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(msg, t)
    }

    override fun trace(msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(msg.toStringSafe())
    }

    override fun trace(t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(msg.toStringSafe(), t)
    }

    fun trace(arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    override fun isTraceEnabled(marker: Marker): Boolean
    {
        return underlyingLogger.isTraceEnabled(marker)
    }

    override fun trace(marker: Marker, msg: String)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(marker, msg)
    }

    override fun trace(marker: Marker, format: String, arg: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(marker, format, arg)
    }

    override fun trace(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(marker, format, arg1, arg2)
    }

    override fun trace(marker: Marker, format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(marker, format, *arguments)
    }

    override fun trace(marker: Marker, msg: String, t: Throwable)
    {
        if (underlyingLogger.isTraceEnabled && isTimeToLog(TRACE)) underlyingLogger.trace(marker, msg, t)
    }

    override fun trace(marker: Marker?, msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled(marker) && isTimeToLog(TRACE)) underlyingLogger.trace(marker, msg.toStringSafe())
    }

    override fun trace(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled(marker) && isTimeToLog(TRACE)) underlyingLogger.trace(marker, msg.toStringSafe(), t)
    }

    fun trace(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isTraceEnabled(marker) && isTimeToLog(TRACE)) underlyingLogger.trace(marker, msg.toStringSafe(), (arguments.invoke()
                                                                                                                               ?: emptyArray<Any>()))
    }

    // endregion: trace

    // region: DEBUG
    /*
     ___  ___ ___ _   _  ___
    |   \| __| _ ) | | |/ __|
    | |) | _|| _ \ |_| | (_ |
    |___/|___|___/\___/ \___|
    ==========================================================================================================================================================*/

    override fun isDebugEnabled(): Boolean
    {
        return underlyingLogger.isDebugEnabled
    }

    override fun debug(msg: String)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(msg)
    }

    override fun debug(format: String, arg: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(format, arg)
    }

    override fun debug(format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(format, arg1, arg2)
    }

    override fun debug(format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(format, *arguments)
    }

    override fun debug(msg: String, t: Throwable)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(msg, t)
    }

    override fun debug(msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(msg.toStringSafe())
    }

    override fun debug(t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(msg.toStringSafe(), t)
    }

    fun debug(arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    override fun isDebugEnabled(marker: Marker): Boolean
    {
        return underlyingLogger.isDebugEnabled(marker)
    }

    override fun debug(marker: Marker, msg: String)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, msg)
    }

    override fun debug(marker: Marker, format: String, arg: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, format, arg)
    }

    override fun debug(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, format, arg1, arg2)
    }

    override fun debug(marker: Marker, format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, format, *arguments)
    }

    override fun debug(marker: Marker, msg: String, t: Throwable)
    {
        if (underlyingLogger.isDebugEnabled && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, msg, t)
    }

    override fun debug(marker: Marker?, msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled(marker) && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, msg.toStringSafe())
    }

    override fun debug(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled(marker) && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, msg.toStringSafe(), t)
    }

    fun debug(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isDebugEnabled(marker) && isTimeToLog(DEBUG)) underlyingLogger.debug(marker, msg.toStringSafe(), (arguments.invoke()
                                                                                                                               ?: emptyArray<Any>()))
    }
    // endregion: debug

    // region: INFO
    /*
     ___ _  _ ___ ___
    |_ _| \| | __/ _ \
     | || .` | _| (_) |
    |___|_|\_|_| \___/
    ==========================================================================================================================================================*/

    override fun isInfoEnabled(): Boolean
    {
        return underlyingLogger.isInfoEnabled
    }

    override fun info(msg: String)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(msg)
    }

    override fun info(format: String, arg: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(format, arg)
    }

    override fun info(format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(format, arg1, arg2)
    }

    override fun info(format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(format, *arguments)
    }

    override fun info(msg: String, t: Throwable)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(msg, t)
    }

    override fun info(msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(msg.toStringSafe())
    }

    override fun info(t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(msg.toStringSafe(), t)
    }

    fun info(arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    override fun isInfoEnabled(marker: Marker): Boolean
    {
        return underlyingLogger.isInfoEnabled(marker)
    }

    override fun info(marker: Marker, msg: String)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(marker, msg)
    }

    override fun info(marker: Marker, format: String, arg: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(marker, format, arg)
    }

    override fun info(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(marker, format, arg1, arg2)
    }

    override fun info(marker: Marker, format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(marker, format, *arguments)
    }

    override fun info(marker: Marker, msg: String, t: Throwable)
    {
        if (underlyingLogger.isInfoEnabled && isTimeToLog(INFO)) underlyingLogger.info(marker, msg, t)
    }

    override fun info(marker: Marker?, msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled(marker) && isTimeToLog(INFO)) underlyingLogger.info(marker, msg.toStringSafe())
    }

    override fun info(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled(marker) && isTimeToLog(INFO)) underlyingLogger.info(marker, msg.toStringSafe(), t)
    }

    fun info(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isInfoEnabled(marker) && isTimeToLog(INFO)) underlyingLogger.info(marker, msg.toStringSafe(), (arguments.invoke()
                                                                                                                               ?: emptyArray<Any>()))
    }

    //endregion: info

    // region: WARN
    /*
    __      ___   ___ _  _
    \ \    / /_\ | _ \ \| |
     \ \/\/ / _ \|   / .` |
      \_/\_/_/ \_\_|_\_|\_|
    ==========================================================================================================================================================*/

    override fun isWarnEnabled(): Boolean
    {
        return underlyingLogger.isWarnEnabled
    }

    override fun warn(msg: String)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(msg)
    }

    override fun warn(format: String, arg: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(format, arg)
    }

    override fun warn(format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(format, *arguments)
    }

    override fun warn(format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(format, arg1, arg2)
    }

    override fun warn(msg: String, t: Throwable)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(msg, t)
    }

    override fun warn(msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(msg.toStringSafe())
    }

    override fun warn(t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(msg.toStringSafe(), t)
    }

    fun warn(arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    override fun isWarnEnabled(marker: Marker): Boolean
    {
        return underlyingLogger.isWarnEnabled(marker)
    }

    override fun warn(marker: Marker, msg: String)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(marker, msg)
    }

    override fun warn(marker: Marker, format: String, arg: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(marker, format, arg)
    }

    override fun warn(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(marker, format, arg1, arg2)
    }

    override fun warn(marker: Marker, format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(marker, format, *arguments)
    }

    override fun warn(marker: Marker, msg: String, t: Throwable)
    {
        if (underlyingLogger.isWarnEnabled && isTimeToLog(WARN)) underlyingLogger.warn(marker, msg, t)
    }

    override fun warn(marker: Marker?, msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled(marker) && isTimeToLog(WARN)) underlyingLogger.warn(marker, msg.toStringSafe())
    }

    override fun warn(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled(marker) && isTimeToLog(WARN)) underlyingLogger.warn(marker, msg.toStringSafe(), t)
    }

    fun warn(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isWarnEnabled(marker) && isTimeToLog(WARN)) underlyingLogger.warn(marker, msg.toStringSafe(), (arguments.invoke()
                                                                                                                               ?: emptyArray<Any>()))
    }

    //endregion: warn

    // region: ERROR
    /*
     ___ ___ ___  ___  ___
    | __| _ \ _ \/ _ \| _ \
    | _||   /   / (_) |   /
    |___|_|_\_|_\\___/|_|_\
    ==========================================================================================================================================================*/

    override fun isErrorEnabled(): Boolean
    {
        return underlyingLogger.isErrorEnabled
    }

    override fun error(msg: String)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(msg)
    }

    override fun error(format: String, arg: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(format, arg)
    }

    override fun error(format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(format, arg1, arg2)
    }

    override fun error(format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(format, *arguments)
    }

    override fun error(msg: String, t: Throwable)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(msg, t)
    }

    override fun error(msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(msg.toStringSafe())
    }

    override fun error(t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(msg.toStringSafe(), t)
    }

    fun error(arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    override fun isErrorEnabled(marker: Marker): Boolean
    {
        return underlyingLogger.isErrorEnabled(marker)
    }

    override fun error(marker: Marker, msg: String)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(marker, msg)
    }

    override fun error(marker: Marker, format: String, arg: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(marker, format, arg)
    }

    override fun error(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(marker, format, arg1, arg2)
    }

    override fun error(marker: Marker, format: String, vararg arguments: Any)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(marker, format, *arguments)
    }

    override fun error(marker: Marker, msg: String, t: Throwable)
    {
        if (underlyingLogger.isErrorEnabled && isTimeToLog(ERROR)) underlyingLogger.error(marker, msg, t)
    }

    override fun error(marker: Marker?, msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled(marker) && isTimeToLog(ERROR)) underlyingLogger.error(marker, msg.toStringSafe())
    }

    override fun error(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled(marker) && isTimeToLog(ERROR)) underlyingLogger.error(marker, msg.toStringSafe(), t)
    }

    fun error(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        if (underlyingLogger.isErrorEnabled(marker) && isTimeToLog(ERROR)) underlyingLogger.error(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
    }

    // endregion: error

    // region: LOG AT
    /*
     _    ___   ___     _ _____
    | |  / _ \ / __|   /_\_   _|
    | |_| (_) | (_ |  / _ \| |
    |____\___/ \___| /_/ \_\_|
    ==========================================================================================================================================================*/

    @API
    fun logAt(level: Level, msg: String)
    {
        when (level)
        {
            TRACE -> trace(msg)
            DEBUG -> debug(msg)
            INFO  -> info(msg)
            WARN  -> warn(msg)
            ERROR -> error(msg)
        }
    }


    @API
    fun logAt(level: Level, format: String, arg: Any)
    {
        when (level)
        {
            TRACE -> trace(format, arg)
            DEBUG -> debug(format, arg)
            INFO  -> info(format, arg)
            WARN  -> warn(format, arg)
            ERROR -> error(format, arg)
        }
    }


    @API
    fun logAt(level: Level, format: String, arg1: Any, arg2: Any)
    {
        when (level)
        {
            TRACE -> trace(format, arg1, arg2)
            DEBUG -> debug(format, arg1, arg2)
            INFO  -> info(format, arg1, arg2)
            WARN  -> warn(format, arg1, arg2)
            ERROR -> error(format, arg1, arg2)
        }
    }


    @API
    fun logAt(level: Level, format: String, vararg arguments: Any)
    {
        when (level)
        {
            TRACE -> trace(format, *arguments)
            DEBUG -> debug(format, *arguments)
            INFO  -> info(format, *arguments)
            WARN  -> warn(format, *arguments)
            ERROR -> error(format, *arguments)
        }
    }


    @API
    fun logAt(level: Level, msg: String, t: Throwable)
    {
        when (level)
        {
            TRACE -> trace(msg, t)
            DEBUG -> debug(msg, t)
            INFO  -> info(msg, t)
            WARN  -> warn(msg, t)
            ERROR -> error(msg, t)
        }
    }

    @API
    fun logAt(level: Level, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(msg)
            DEBUG -> debug(msg)
            INFO  -> info(msg)
            WARN  -> warn(msg)
            ERROR -> error(msg)
        }
    }


    @API
    fun logAt(level: Level, t: Throwable?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(t, msg)
            DEBUG -> debug(t, msg)
            INFO  -> info(t, msg)
            WARN  -> warn(t, msg)
            ERROR -> error(t, msg)
        }
    }

    @API
    fun logAt(level: Level, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(arguments, msg)
            DEBUG -> debug(arguments, msg)
            INFO  -> info(arguments, msg)
            WARN  -> warn(arguments, msg)
            ERROR -> error(arguments, msg)
        }
    }

    @API
    fun logAt(level: Level, marker: Marker, msg: String)
    {
        when (level)
        {
            TRACE -> trace(marker, msg)
            DEBUG -> debug(marker, msg)
            INFO  -> info(marker, msg)
            WARN  -> warn(marker, msg)
            ERROR -> error(marker, msg)
        }
    }


    @API
    fun logAt(level: Level, marker: Marker, format: String, arg: Any)
    {
        when (level)
        {
            TRACE -> trace(marker, format, arg)
            DEBUG -> debug(marker, format, arg)
            INFO  -> info(marker, format, arg)
            WARN  -> warn(marker, format, arg)
            ERROR -> error(marker, format, arg)
        }
    }


    @API
    fun logAt(level: Level, marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        when (level)
        {
            TRACE -> trace(marker, format, arg1, arg2)
            DEBUG -> debug(marker, format, arg1, arg2)
            INFO  -> info(marker, format, arg1, arg2)
            WARN  -> warn(marker, format, arg1, arg2)
            ERROR -> error(marker, format, arg1, arg2)
        }
    }


    @API
    fun logAt(level: Level, marker: Marker, format: String, vararg arguments: Any)
    {
        when (level)
        {
            TRACE -> trace(marker, format, *arguments)
            DEBUG -> debug(marker, format, *arguments)
            INFO  -> info(marker, format, *arguments)
            WARN  -> warn(marker, format, *arguments)
            ERROR -> error(marker, format, *arguments)
        }
    }


    @API
    fun logAt(level: Level, marker: Marker, msg: String, t: Throwable)
    {
        when (level)
        {
            TRACE -> trace(marker, msg, t)
            DEBUG -> debug(marker, msg, t)
            INFO  -> info(marker, msg, t)
            WARN  -> warn(marker, msg, t)
            ERROR -> error(marker, msg, t)
        }
    }


    @API
    fun logAt(level: Level, marker: Marker?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(marker, msg)
            DEBUG -> debug(marker, msg)
            INFO  -> info(marker, msg)
            WARN  -> warn(marker, msg)
            ERROR -> error(marker, msg)
        }
    }

    @API
    fun logAt(level: Level, marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(marker, t, msg)
            DEBUG -> debug(marker, t, msg)
            INFO  -> info(marker, t, msg)
            WARN  -> warn(marker, t, msg)
            ERROR -> error(marker, t, msg)
        }
    }

    @API
    fun logAt(level: Level, marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> trace(marker, arguments, msg)
            DEBUG -> debug(marker, arguments, msg)
            INFO  -> info(marker, arguments, msg)
            WARN  -> warn(marker, arguments, msg)
            ERROR -> error(marker, arguments, msg)
        }
    }

    // endregion: logAt

    // region: MISC
    /** Add a log message with all the supplied parameters along with method name. */
    override fun entry(vararg argArray: Any?)
    {
        if (underlyingLogger.isTraceEnabled(ENTRY) && isTimeToLog(TRACE))
        {
            val tp = MessageFormatter.arrayFormat(buildMessagePattern(argArray.size), argArray)
            underlyingLogger.trace(ENTRY, tp.message)
        }
    }

    /** Add log message indicating exit of a method. */
    override fun exit()
    {
        if (underlyingLogger.isTraceEnabled(EXIT) && isTimeToLog(TRACE))
        {
            underlyingLogger.trace(EXIT, EXIT_ONLY)
        }
    }

    /** Add a log message with the return value of a method. */
    override fun <T> exit(result: T): T
    {
        if (underlyingLogger.isTraceEnabled(EXIT) && isTimeToLog(TRACE))
        {
            val tp = MessageFormatter.format(EXIT_MESSAGE, result)
            underlyingLogger.trace(EXIT, tp.message, arrayOf<Any?>(result), tp.throwable)
        }
        return result
    }

    /** Add a log message at teh `ERROR` level indicating an exception is caught along with the stack trace. */
    override fun <T : Throwable> catching(throwable: T)
    {
        if (underlyingLogger.isErrorEnabled(CATCHING) && isTimeToLog(ERROR))
        {
            underlyingLogger.error(CATCHING, "catching", throwable)
        }
    }

    /** Add a log message indicating an exception will be thrown along with the stack trace. */
    override fun <T : Throwable> throwing(throwable: T): T
    {
        underlyingLogger.error(THROWING,"throwing", throwable)
        throw throwable
    }
    // endregion: misc

    private fun buildMessagePattern(len: Int): String
    {
        val sb = StringBuilder()
        sb.append(" entry with (")
        for (i in 0 until len)
        {
            sb.append("{}")
            if (i != len - 1) sb.append(", ")
        }
        sb.append(')')
        return sb.toString()
    }
}

/**
 * A [LaconicLogger] that is preconfigured as to the level it logs at. **Use the various `log()` functions** (not to be confused with the
 * `logAt()` functions) to log at the configured level. Ironically, this comes at a extremely small performance
 * penalty. See the [LaconicLogger] documentation for more information the purpose and operation of a `LaconicLogger`.
 */
@Suppress("DuplicatedCode")
class LaconicLeveledLogger @JvmOverloads constructor(underlyingLogger: Logger, interval: TimeDuration, private val level: Level = DEBUG) : LaconicLogger(underlyingLogger, interval)
{
    @API
    constructor(logger: Logger, intervalDuration: Long, intervalTimeUnit: TimeUnit, level: Level = DEBUG) : this(KotlinLogging.logger(logger.name), TimeDuration.of(intervalDuration, intervalTimeUnit), level)

    @API
    constructor(logger: Logger, interval: String, level: Level = DEBUG) : this(KotlinLogging.logger(logger.name), TimeDuration.of(interval), level)


    private val laconicLogger = LaconicLogger(underlyingLogger, interval)

    @API
    fun log(msg: String)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(msg)
            DEBUG -> laconicLogger.debug(msg)
            INFO  -> laconicLogger.info(msg)
            WARN  -> laconicLogger.warn(msg)
            ERROR -> laconicLogger.error(msg)
        }
    }


    @API
    fun log(format: String, arg: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(format, arg)
            DEBUG -> laconicLogger.debug(format, arg)
            INFO  -> laconicLogger.info(format, arg)
            WARN  -> laconicLogger.warn(format, arg)
            ERROR -> laconicLogger.error(format, arg)
        }
    }


    @API
    fun log(format: String, arg1: Any, arg2: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(format, arg1, arg2)
            DEBUG -> laconicLogger.debug(format, arg1, arg2)
            INFO  -> laconicLogger.info(format, arg1, arg2)
            WARN  -> laconicLogger.warn(format, arg1, arg2)
            ERROR -> laconicLogger.error(format, arg1, arg2)
        }
    }


    @API
    fun log(format: String, vararg arguments: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(format, *arguments)
            DEBUG -> laconicLogger.debug(format, *arguments)
            INFO  -> laconicLogger.info(format, *arguments)
            WARN  -> laconicLogger.warn(format, *arguments)
            ERROR -> laconicLogger.error(format, *arguments)
        }
    }


    @API
    fun log(msg: String, t: Throwable)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(msg, t)
            DEBUG -> laconicLogger.debug(msg, t)
            INFO  -> laconicLogger.info(msg, t)
            WARN  -> laconicLogger.warn(msg, t)
            ERROR -> laconicLogger.error(msg, t)
        }
    }

    @API
    fun log(msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(msg)
            DEBUG -> laconicLogger.debug(msg)
            INFO  -> laconicLogger.info(msg)
            WARN  -> laconicLogger.warn(msg)
            ERROR -> laconicLogger.error(msg)
        }
    }


    @API
    fun log(t: Throwable?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(t, msg)
            DEBUG -> laconicLogger.debug(t, msg)
            INFO  -> laconicLogger.info(t, msg)
            WARN  -> laconicLogger.warn(t, msg)
            ERROR -> laconicLogger.error(t, msg)
        }
    }


    @API
    fun log(marker: Marker, msg: String)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, msg)
            DEBUG -> laconicLogger.debug(marker, msg)
            INFO  -> laconicLogger.info(marker, msg)
            WARN  -> laconicLogger.warn(marker, msg)
            ERROR -> laconicLogger.error(marker, msg)
        }
    }


    @API
    fun log(marker: Marker, format: String, arg: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, format, arg)
            DEBUG -> laconicLogger.debug(marker, format, arg)
            INFO  -> laconicLogger.info(marker, format, arg)
            WARN  -> laconicLogger.warn(marker, format, arg)
            ERROR -> laconicLogger.error(marker, format, arg)
        }
    }


    @API
    fun log(marker: Marker, format: String, arg1: Any, arg2: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, format, arg1, arg2)
            DEBUG -> laconicLogger.debug(marker, format, arg1, arg2)
            INFO  -> laconicLogger.info(marker, format, arg1, arg2)
            WARN  -> laconicLogger.warn(marker, format, arg1, arg2)
            ERROR -> laconicLogger.error(marker, format, arg1, arg2)
        }
    }


    @API
    fun log(marker: Marker, format: String, vararg arguments: Any)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, format, *arguments)
            DEBUG -> laconicLogger.debug(marker, format, *arguments)
            INFO  -> laconicLogger.info(marker, format, *arguments)
            WARN  -> laconicLogger.warn(marker, format, *arguments)
            ERROR -> laconicLogger.error(marker, format, *arguments)
        }
    }


    @API
    fun log(marker: Marker, msg: String, t: Throwable)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, msg, t)
            DEBUG -> laconicLogger.debug(marker, msg, t)
            INFO  -> laconicLogger.info(marker, msg, t)
            WARN  -> laconicLogger.warn(marker, msg, t)
            ERROR -> laconicLogger.error(marker, msg, t)
        }
    }


    @API
    fun log(marker: Marker?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, msg)
            DEBUG -> laconicLogger.debug(marker, msg)
            INFO  -> laconicLogger.info(marker, msg)
            WARN  -> laconicLogger.warn(marker, msg)
            ERROR -> laconicLogger.error(marker, msg)
        }
    }

    @API
    fun log(marker: Marker?, t: Throwable?, msg: () -> Any?)
    {
        when (level)
        {
            TRACE -> laconicLogger.trace(marker, t, msg)
            DEBUG -> laconicLogger.debug(marker, t, msg)
            INFO  -> laconicLogger.info(marker, t, msg)
            WARN  -> laconicLogger.warn(marker, t, msg)
            ERROR -> laconicLogger.error(marker, t, msg)
        }
    }
}
