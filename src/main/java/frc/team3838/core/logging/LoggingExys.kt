@file:Suppress("unused", "DuplicatedCode")

package frc.team3838.core.logging

import frc.team3838.core.meta.API
import org.slf4j.Logger
import org.slf4j.Marker
import org.slf4j.event.Level


@Suppress("NOTHING_TO_INLINE")
internal inline fun (() -> Any?).toStringSafe(): String
{
    return try
    {
        invoke().toString()
    }
    catch (e: Exception)
    {
        "Log message invocation failed: $e"
    }
}

// region: TRACE
/*
 _____ ___    _   ___ ___
|_   _| _ \  /_\ / __| __|
  | | |   / / _ \ (__| _|
  |_| |_|_\/_/ \_\___|___|
==========================================================================================================================================================*/

fun Logger.trace(msg: () -> Any?)
{
    if (this.isTraceEnabled) this.trace(msg.toStringSafe())
}

fun Logger.trace(t: Throwable, msg: () -> Any?)
{
    if (this.isTraceEnabled) this.trace(msg.toStringSafe(), t)
}

fun Logger.trace(arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isTraceEnabled) this.trace(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

fun Logger.trace(marker: Marker?, msg: () -> Any?)
{
    if (this.isTraceEnabled(marker)) this.trace(marker, msg.toStringSafe())
}

fun Logger.trace(marker: Marker?, t: Throwable, msg: () -> Any?)
{
    if (this.isTraceEnabled(marker)) this.trace(marker, msg.toStringSafe(), t)
}

fun Logger.trace(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isTraceEnabled(marker)) this.trace(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

// endregion: trace

// region: DEBUG
/*
 ___  ___ ___ _   _  ___
|   \| __| _ ) | | |/ __|
| |) | _|| _ \ |_| | (_ |
|___/|___|___/\___/ \___|
==========================================================================================================================================================*/

fun Logger.debug(msg: () -> Any?)
{
    if (this.isDebugEnabled) this.debug(msg.toStringSafe())
}

fun Logger.debug(t: Throwable, msg: () -> Any?)
{
    if (this.isDebugEnabled) this.debug(msg.toStringSafe(), t)
}

fun Logger.debug(arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isDebugEnabled) this.debug(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

fun Logger.debug(marker: Marker?, msg: () -> Any?)
{
    if (this.isDebugEnabled(marker)) this.debug(marker, msg.toStringSafe())
}

fun Logger.debug(marker: Marker?, t: Throwable, msg: () -> Any?)
{
    if (this.isDebugEnabled(marker)) this.debug(marker, msg.toStringSafe(), t)
}

fun Logger.debug(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isDebugEnabled(marker)) this.debug(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

// endregion: debug

// region: INFO
/*
 ___ _  _ ___ ___
|_ _| \| | __/ _ \
 | || .` | _| (_) |
|___|_|\_|_| \___/
==========================================================================================================================================================*/

fun Logger.info(msg: () -> Any?)
{
    if (this.isInfoEnabled) this.info(msg.toStringSafe())
}

fun Logger.info(t: Throwable, msg: () -> Any?)
{
    if (this.isInfoEnabled) this.info(msg.toStringSafe(), t)
}

fun Logger.info(arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isInfoEnabled) this.info(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

fun Logger.info(marker: Marker?, msg: () -> Any?)
{
    if (this.isInfoEnabled(marker)) this.info(marker, msg.toStringSafe())
}

fun Logger.info(marker: Marker?, t: Throwable, msg: () -> Any?)
{
    if (this.isInfoEnabled(marker)) this.info(marker, msg.toStringSafe(), t)
}

fun Logger.info(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isInfoEnabled(marker)) this.info(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

//endregion: info

// region: WARN
/*
__      ___   ___ _  _
\ \    / /_\ | _ \ \| |
 \ \/\/ / _ \|   / .` |
  \_/\_/_/ \_\_|_\_|\_|
==========================================================================================================================================================*/

fun Logger.warn(msg: () -> Any?)
{
    if (this.isWarnEnabled) this.warn(msg.toStringSafe())
}

fun Logger.warn(t: Throwable, msg: () -> Any?)
{
    if (this.isWarnEnabled) this.warn(msg.toStringSafe(), t)
}

fun Logger.warn(arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isWarnEnabled) this.warn(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

fun Logger.warn(marker: Marker?, msg: () -> Any?)
{
    if (this.isWarnEnabled(marker)) this.warn(marker, msg.toStringSafe())
}

fun Logger.warn(marker: Marker?, t: Throwable, msg: () -> Any?)
{
    if (this.isWarnEnabled(marker)) this.warn(marker, msg.toStringSafe(), t)
}

fun Logger.warn(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isWarnEnabled(marker)) this.warn(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

//endregion: warn

// region: ERROR
/*
 ___ ___ ___  ___  ___
| __| _ \ _ \/ _ \| _ \
| _||   /   / (_) |   /
|___|_|_\_|_\\___/|_|_\
==========================================================================================================================================================*/

fun Logger.error(msg: () -> Any?)
{
    if (this.isErrorEnabled) this.error(msg.toStringSafe())
}

fun Logger.error(t: Throwable, msg: () -> Any?)
{
    if (this.isErrorEnabled) this.error(msg.toStringSafe(), t)
}

fun Logger.error(arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isErrorEnabled) this.error(msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

fun Logger.error(marker: Marker?, msg: () -> Any?)
{
    if (this.isErrorEnabled(marker)) this.error(marker, msg.toStringSafe())
}

fun Logger.error(marker: Marker?, t: Throwable, msg: () -> Any?)
{
    if (this.isErrorEnabled(marker)) this.error(marker, msg.toStringSafe(), t)
}

fun Logger.error(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    if (this.isErrorEnabled(marker)) this.error(marker, msg.toStringSafe(), (arguments.invoke() ?: emptyArray<Any>()))
}

// endregion: error

// region: LOG AT
/*
 _    ___   ___     _ _____
| |  / _ \ / __|   /_\_   _|
| |_| (_) | (_ |  / _ \| |
|____\___/ \___| /_/ \_\_|
==========================================================================================================================================================*/

fun Logger.isEnabledAt(level: Level?): Boolean
{
    return when (level)
    {
        null -> false
        Level.TRACE -> this.isTraceEnabled
        Level.DEBUG -> this.isDebugEnabled
        Level.INFO  -> this.isInfoEnabled
        Level.WARN  -> this.isWarnEnabled
        Level.ERROR -> this.isErrorEnabled
    }
}

fun Logger.isEnabledAt(level: Level?, marker: Marker): Boolean
{
    return when (level)
    {
        null -> false
        Level.TRACE -> this.isTraceEnabled(marker)
        Level.DEBUG -> this.isDebugEnabled(marker)
        Level.INFO  -> this.isInfoEnabled(marker)
        Level.WARN  -> this.isWarnEnabled(marker)
        Level.ERROR -> this.isErrorEnabled(marker)
    }
}


fun Logger.logAt(level: Level?, msg: String)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(msg)
        Level.DEBUG -> this.debug(msg)
        Level.INFO  -> this.info(msg)
        Level.WARN  -> this.warn(msg)
        Level.ERROR -> this.error(msg)
    }
}

fun Logger.logAt(level: Level?, format: String, arg: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(format, arg)
        Level.DEBUG -> this.debug(format, arg)
        Level.INFO  -> this.info(format, arg)
        Level.WARN  -> this.warn(format, arg)
        Level.ERROR -> this.error(format, arg)
    }
}

fun Logger.logAt(level: Level?, format: String, arg1: Any, arg2: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(format, arg1, arg2)
        Level.DEBUG -> this.debug(format, arg1, arg2)
        Level.INFO  -> this.info(format, arg1, arg2)
        Level.WARN  -> this.warn(format, arg1, arg2)
        Level.ERROR -> this.error(format, arg1, arg2)
    }
}

fun Logger.logAt(level: Level?, format: String, vararg arguments: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(format, *arguments)
        Level.DEBUG -> this.debug(format, *arguments)
        Level.INFO  -> this.info(format, *arguments)
        Level.WARN  -> this.warn(format, *arguments)
        Level.ERROR -> this.error(format, *arguments)
    }
}

fun Logger.logAt(level: Level?, msg: String, t: Throwable)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(msg, t)
        Level.DEBUG -> this.debug(msg, t)
        Level.INFO  -> this.info(msg, t)
        Level.WARN  -> this.warn(msg, t)
        Level.ERROR -> this.error(msg, t)
    }
}

@API
fun Logger.logAt(level: Level?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(arguments, msg)
        Level.DEBUG -> this.debug(arguments, msg)
        Level.INFO  -> this.info(arguments, msg)
        Level.WARN  -> this.warn(arguments, msg)
        Level.ERROR -> this.error(arguments, msg)
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, msg: String)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, msg)
        Level.DEBUG -> this.debug(marker, msg)
        Level.INFO  -> this.info(marker, msg)
        Level.WARN  -> this.warn(marker, msg)
        Level.ERROR -> this.error(marker, msg)
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, format: String, arg: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, format, arg)
        Level.DEBUG -> this.debug(marker, format, arg)
        Level.INFO  -> this.info(marker, format, arg)
        Level.WARN  -> this.warn(marker, format, arg)
        Level.ERROR -> this.error(marker, format, arg)
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, format: String, arg1: Any, arg2: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, format, arg1, arg2)
        Level.DEBUG -> this.debug(marker, format, arg1, arg2)
        Level.INFO  -> this.info(marker, format, arg1, arg2)
        Level.WARN  -> this.warn(marker, format, arg1, arg2)
        Level.ERROR -> this.error(marker, format, arg1, arg2)
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, format: String, vararg arguments: Any)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, format, *arguments)
        Level.DEBUG -> this.debug(marker, format, *arguments)
        Level.INFO  -> this.info(marker, format, *arguments)
        Level.WARN  -> this.warn(marker, format, *arguments)
        Level.ERROR -> this.error(marker, format, *arguments)
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, msg: String, t: Throwable)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, msg, t)
        Level.DEBUG -> this.debug(marker, msg, t)
        Level.INFO  -> this.info(marker, msg, t)
        Level.WARN  -> this.warn(marker, msg, t)
        Level.ERROR -> this.error(marker, msg, t)
    }
}


fun Logger.logAt(level: Level?, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace { msg }
        Level.DEBUG -> this.debug { msg }
        Level.INFO  -> this.info { msg }
        Level.WARN  -> this.warn { msg }
        Level.ERROR -> this.error { msg }
    }
}

fun Logger.logAt(level: Level?, t: Throwable, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(t) { msg }
        Level.DEBUG -> this.debug(t) { msg }
        Level.INFO  -> this.info(t) { msg }
        Level.WARN  -> this.warn(t) { msg }
        Level.ERROR -> this.error(t) { msg }
    }
}


fun Logger.logAt(level: Level?, marker: Marker?, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker) { msg }
        Level.DEBUG -> this.debug(marker) { msg }
        Level.INFO  -> this.info(marker) { msg }
        Level.WARN  -> this.warn(marker) { msg }
        Level.ERROR -> this.error(marker) { msg }
    }
}

fun Logger.logAt(level: Level?, marker: Marker?, t: Throwable, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        Level.TRACE -> this.trace(marker, t) { msg }
        Level.DEBUG -> this.debug(marker, t) { msg }
        Level.INFO  -> this.info(marker, t) { msg }
        Level.WARN  -> this.warn(marker, t) { msg }
        Level.ERROR -> this.error(marker, t) { msg }
    }
}

@API
fun Logger.logAt(level: Level?, marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
{
    when (level)
    {
        null -> return
        // fun Logger.trace(marker: Marker?, arguments: () -> Array<Any>?, msg: () -> Any?)
        Level.TRACE -> this.trace(marker, arguments, msg)
        Level.DEBUG -> this.debug(marker, arguments, msg)
        Level.INFO  -> this.info(marker, arguments, msg)
        Level.WARN  -> this.warn(marker, arguments, msg)
        Level.ERROR -> this.error(marker, arguments, msg)
    }
}

// endregion: logAt


// region: IF ENABLED FUNCTIONS

/**
 * Lazily evaluates the supplied function if `isTraceEnabled` evaluates `true` for the receiver logger.
 *
 * @param marker the optional [Marker] to take into consideration when evaluating `isTraceEnabled`
 */
@JvmOverloads
fun Logger.ifTraceIsEnabled(marker: Marker? = null, action: () -> Unit)
{
    if (this.isTraceEnabled(marker))
    {
        action.invoke()
    }
}

/**
 * Lazily evaluates the supplied function if `isDebugEnabled` evaluates `true` for the receiver logger.
 *
 * @param marker the optional [Marker] to take into consideration when evaluating `isDebugEnabled`
 */
@JvmOverloads
fun Logger.ifDebugIsEnabled(marker: Marker? = null, action: () -> Unit)
{
    if (this.isDebugEnabled(marker))
    {
        action.invoke()
    }
}

/**
 * Lazily evaluates the supplied function if `isInfoEnabled` evaluates `true` for the receiver logger.
 *
 * @param marker the optional [Marker] to take into consideration when evaluating `isInfoEnabled`
 */@JvmOverloads
fun Logger.ifInfoIsEnabled(marker: Marker? = null, action: () -> Unit)
{
    if (this.isInfoEnabled(marker))
    {
        action.invoke()
    }
}

/**
 * Lazily evaluates the supplied function if `isWarnEnabled` evaluates `true` for the receiver logger.
 *
 * @param marker the optional [Marker] to take into consideration when evaluating `isWarnEnabled`
 */
@JvmOverloads
fun Logger.ifWarnIsEnabled(marker: Marker? = null, action: () -> Unit)
{
    if (this.isWarnEnabled(marker))
    {
        action.invoke()
    }
}

/**
 * Lazily evaluates the supplied function if `isErrorEnabled` evaluates `true` for the receiver logger.
 *
 * @param marker the optional [Marker] to take into consideration when evaluating `isErrorEnabled`
 */
@JvmOverloads
fun Logger.ifErrorIsEnabled(marker: Marker? = null, action: () -> Unit)
{
    if (this.isErrorEnabled(marker))
    {
        action.invoke()
    }
}

// endregion: ifEnabled