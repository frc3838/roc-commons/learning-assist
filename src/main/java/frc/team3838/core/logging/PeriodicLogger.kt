package frc.team3838.core.logging

import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import org.slf4j.Logger
import org.slf4j.Marker
import org.slf4j.event.Level
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


/**
 * A logging utility that will periodically log a message at fixed intervals.
 */
class PeriodicLogger @JvmOverloads constructor(underlyingLogger: Logger, level: Level, marker: Marker?, interval: TimeDuration, waitToLogFirst: Boolean = true, var msg: () -> String)
{
    @JvmOverloads
    constructor(underlyingLogger: Logger, level: Level, interval: TimeDuration, waitToLogFirst: Boolean = true, msg: () -> String): this(underlyingLogger, level, null, interval, waitToLogFirst, msg)

    private val scheduler = Executors.newScheduledThreadPool(1)

    private val logTask: Runnable = Runnable {
        if (marker != null) underlyingLogger.logAt(level, marker, msg.invoke())
        else underlyingLogger.logAt(level, msg.invoke())
    }

    init
    {
        val initialDelay = if (waitToLogFirst) interval.toNanos() else 0L
        scheduler.scheduleAtFixedRate(logTask, initialDelay, interval.toNanos(), TimeUnit.NANOSECONDS)
    }

    @API
    fun shutdown()
    {
        try
        {
            scheduler.shutdown()
        }
        catch (ignore: Exception)
        {
            try
            {
                scheduler.shutdownNow()
            }
            catch (ignore: Exception) {}
        }
    }
}