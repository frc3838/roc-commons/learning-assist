package frc.team3838.core.logging

import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import org.slf4j.Logger
import org.slf4j.event.Level


abstract class SmartLogMessage(private val logger: Logger,
                               private val firstTimeLevel: Level,
                               private val laconicLevel: Level,
                               private val laconicLoggingInterval: TimeDuration,
                               private val dynamicMessage: Boolean)
{
    var initialMessageLogged = false
        private set

    private val message: String by lazy { createMessage() }

    private val laconicLogger: LaconicLogger by lazy { LaconicLogger(logger, laconicLoggingInterval) }

    @API
    fun logMessage()
    {
        if (!initialMessageLogged)
        {
            initialMessageLogged = true
            laconicLogger.underlyingLogger.logAt(firstTimeLevel) { if (dynamicMessage) createMessage() else message }
        }
        else
        {
            laconicLogger.logAt(laconicLevel, (if (dynamicMessage) createMessage() else message))
        }
    }

    /**
     * Used to create the log message. If [dynamicMessage] is set to false, it is called lazily one time
     * when the initial message is logged. Otherwise it is called each time the message is logged (laconically).
     */
    abstract fun createMessage(): String
}