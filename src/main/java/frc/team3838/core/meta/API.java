package frc.team3838.core.meta;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;



/**
 * <p>
 * A marker annotation to indicate that a method or class is a public API and therefore may be
 * unused in the project. IntelliJ IDEA can be configured to use this annotation
 * so that unused warnings are suppressed for the method, but not for other things
 * within the method as would be the case if the {@code @SuppressWarnings("unused")}
 * annotation was used.
 * </p>
 *
 * <p>
 * <strong>To configure IntelliJ IDEA</strong>
 * </p>
 * <ol>
 * <li>Go to <em>Settings | Editor | Inspections</em></li>
 * <li>Go to the <em>Declaration Redundancy | Unused declaration</em> inspection</li>
 * <li>On the 'Entry points', click the 'Configure Annotations...' button</li>
 * <li>Click the (top) 'Add Class' icon and enter the fully qualified name of this Annotation</li>
 * </ol>
 * <p>
 * See this answer to this stackoverflow question for more information:
 * <a href='http://stackoverflow.com/a/38899030/1348743'>http://stackoverflow.com/a/38899030/1348743</a>
 * </p>
 *
 */
@Documented
@Retention(RetentionPolicy.CLASS)
public @interface API
{

}
